\begin{thebibliography}{10}
\expandafter\ifx\csname natexlab\endcsname\relax\def\natexlab#1{#1}\fi
\providecommand{\url}[1]{\texttt{#1}}
\providecommand{\href}[2]{#2}
\providecommand{\path}[1]{#1}
\providecommand{\DOIprefix}{doi:}
\providecommand{\ArXivprefix}{arXiv:}
\providecommand{\URLprefix}{URL: }
\providecommand{\Pubmedprefix}{pmid:}
\providecommand{\doi}[1]{\href{http://dx.doi.org/#1}{\path{#1}}}
\providecommand{\Pubmed}[1]{\href{pmid:#1}{\path{#1}}}
\providecommand{\bibinfo}[2]{#2}
\ifx\xfnm\undefined \def\xfnm[#1]{\unskip,\space#1}\fi
%Type = Article
\bibitem[{Brainard and Vision(1997)}]{psychtoolbox97}
\bibinfo{author}{Brainard\xfnm[ D.H.]}, \bibinfo{author}{Vision\xfnm[ S.]}.
\newblock \bibinfo{title}{The psychophysics toolbox}.
\newblock \bibinfo{journal}{Spatial vision}
  \bibinfo{year}{1997};\bibinfo{volume}{10}:\bibinfo{pages}{433--436}.
%Type = Article
\bibitem[{Foerster et~al.(2016)Foerster, Poth, Behler, Botsch and
  Schneider}]{foerster2016VR}
\bibinfo{author}{Foerster\xfnm[ R.M.]}, \bibinfo{author}{Poth\xfnm[ C.H.]},
  \bibinfo{author}{Behler\xfnm[ C.]}, \bibinfo{author}{Botsch\xfnm[ M.]},
  \bibinfo{author}{Schneider\xfnm[ W.X.]}.
\newblock \bibinfo{title}{Using the virtual reality device {O}culus {R}ift for
  neuropsychological assessment of visual processing capabilities}.
\newblock \bibinfo{journal}{Scientific reports}
  \bibinfo{year}{2016};\bibinfo{volume}{6}:\bibinfo{pages}{37016}.
%Type = Misc
\bibitem[{{HTC}(2016)}]{url/HTCVive}
\bibinfo{author}{{HTC}\xfnm[]}.
\newblock \bibinfo{title}{{HTC Vive}}.
\newblock \bibinfo{howpublished}{\url{https://www.vive.com/us/}};
  \bibinfo{year}{2016}.
\newblock \bibinfo{note}{Accessed: 2018-07-18}.
%Type = Article
\bibitem[{Kleiner et~al.(2007)Kleiner, Brainard, Pelli, Ingling, Murray,
  Broussard et~al.}]{psychtoolbox07}
\bibinfo{author}{Kleiner\xfnm[ M.]}, \bibinfo{author}{Brainard\xfnm[ D.]},
  \bibinfo{author}{Pelli\xfnm[ D.]}, \bibinfo{author}{Ingling\xfnm[ A.]},
  \bibinfo{author}{Murray\xfnm[ R.]}, \bibinfo{author}{Broussard\xfnm[ C.]},
  et~al.
\newblock \bibinfo{title}{What’s new in {P}sychtoolbox-3}.
\newblock \bibinfo{journal}{Perception}
  \bibinfo{year}{2007};\bibinfo{volume}{36}(\bibinfo{number}{14}):\bibinfo{pages}{1}.
%Type = Inproceedings
\bibitem[{Mathur et~al.(2017)Mathur, Majumdar and Ghose}]{visual_search3D}
\bibinfo{author}{Mathur\xfnm[ A.S.]}, \bibinfo{author}{Majumdar\xfnm[ R.]},
  \bibinfo{author}{Ghose\xfnm[ T.]}.
\newblock \bibinfo{title}{Study of visual search in 3d space using virtual
  reality (vr)}.
\newblock In: \bibinfo{booktitle}{European Conference on Visual Perception
  (ECVP)}. \bibinfo{year}{2017}. .
%Type = Misc
\bibitem[{{Oculus}(2016)}]{url/Oculus}
\bibinfo{author}{{Oculus}\xfnm[]}.
\newblock \bibinfo{title}{{Oculus Rift}}.
\newblock \bibinfo{howpublished}{\url{https://www.oculus.com/rift/}};
  \bibinfo{year}{2016}.
\newblock \bibinfo{note}{Accessed: 2018-07-18}.
%Type = Article
\bibitem[{Peirce(2007)}]{psychopy2007}
\bibinfo{author}{Peirce\xfnm[ J.W.]}.
\newblock \bibinfo{title}{Psychopy—psychophysics software in {P}ython}.
\newblock \bibinfo{journal}{Journal of neuroscience methods}
  \bibinfo{year}{2007};\bibinfo{volume}{162}(\bibinfo{number}{1-2}):\bibinfo{pages}{8--13}.
%Type = Article
\bibitem[{Peirce(2009)}]{psychopy2009}
\bibinfo{author}{Peirce\xfnm[ J.W.]}.
\newblock \bibinfo{title}{Generating stimuli for neuroscience using
  {P}sycho{P}y}.
\newblock \bibinfo{journal}{Frontiers in neuroinformatics}
  \bibinfo{year}{2009};\bibinfo{volume}{2}:\bibinfo{pages}{10}.
%Type = Article
\bibitem[{Sanchez-Vives and Slater(2005)}]{nature05}
\bibinfo{author}{Sanchez-Vives\xfnm[ M.V.]}, \bibinfo{author}{Slater\xfnm[
  M.]}.
\newblock \bibinfo{title}{From presence to consciousness through virtual
  reality}.
\newblock \bibinfo{journal}{Nature Reviews Neuroscience}
  \bibinfo{year}{2005};\bibinfo{volume}{6}(\bibinfo{number}{4}):\bibinfo{pages}{332}.
%Type = Misc
\bibitem[{{Sony}(2016)}]{url/SonyVR}
\bibinfo{author}{{Sony}\xfnm[]}.
\newblock \bibinfo{title}{{Playstation VR}}.
\newblock
  \bibinfo{howpublished}{\url{https://www.playstation.com/en-us/explore/playstation-vr/}};
  \bibinfo{year}{2016}.
\newblock \bibinfo{note}{Accessed: 2018-07-18}.

\end{thebibliography}
