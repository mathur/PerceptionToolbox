\section{Experiment design with \tool}
\label{sec:experiment_design}
% Describes offerings in terms of program interface, abstractions and Psych specific functionality
This section describes the programming interface and abstractions offered by \tool.
\tool describes an experiment as a collection of \emph{scene}s: \emph{fixation scene}(s), \emph{visual scene}(s) and \emph{response scene}(s), presented one after the other.
\tool also provides easy to use interfaces for describing and placing objects in 3D space, timing and user input handling.
 
\subsection{Abstractions for scenes}
Most experiments consist of three kinds of scenes- fixation scenes, scenes with a visual stimulus and scenes asking for a user response. 
\tool identifies these as separate categories of \emph{scene}s and provides easy and suitable functionality for each \emph{scene} type---especially in the context of VR displays and interactive input devices. 

\subsubsection{Fixation scene}
A \emph{fixation scene} is used to re-orient participants to the center of the room and look in the forward direction. 
This is important as most modern VR devices allow 6 degrees of freedom, i.e., rotation and translation in all three axes. 
During the course of a trial, it is common of participants to stray to some other position and look in a different direction.
A \emph{fixation scene} controls for this.
\tool ensures participants return to the centre of the room and look in a particular direction before progressing to the next \emph{scene} using visual aids inside the \emph{scene} (see Figures \ref{fig:fixation_scene_cross} and \ref{fig:fixation_scene_boxes}).
Code Snippet \ref{code_snippet:fixation_scene} depicts how a \emph{fixation scene} is created using \tool's Python interface.

\begin{lstlisting}[caption=Creating a new experiment and adding a \emph{fixation scene} to it, label=code_snippet:fixation_scene]
# Create a new experiment
exp = experiment.Experiment(name="Sample VR Experiment")
# Make a fixation scene with instructions for the participant
fs = ptvr.stimuli.world.FixationScene(text="Get back to the centre and look at the cross.")
# Add fixation scene to the experiment
exp.add_scene(fs)
\end{lstlisting}

%Screenshot of fixation scene
\begin{figure}[!tbp]
    \centering
    \subfloat[Fixation cross to ensure participants look in that direction]{\includegraphics[width=0.4\textwidth]{images/fixation_scene_cross}\label{fig:fixation_scene_cross}}
  	\hfill
  	\subfloat[Boxes on the floor to help align feet and reach the centre of the room]{\includegraphics[width=0.4\textwidth]{images/fixation_scene_boxes}\label{fig:fixation_scene_boxes}}
  	\caption{Guiding objects used in the \emph{fixation scene}}
\end{figure}

\subsubsection{Visual scene}
A \emph{visual scene} provides a blank canvas (an empty 3D environment) to which some 3D objects (or text) may be added.
The visual stimulus can be presented for a particular time, or until the participant presses a particular button. 
For example, Code Snippet \ref{code_snippet:visual_scene} produces a \emph{scene} as shown in Figure \ref{fig:visual_scene}. 


\begin{lstlisting}[caption=Creating a \emph{visual scene} with some visual objects, label=code_snippet:visual_scene]
# 'cube' will be the visual object used for populating the 3D world
cube = ptvr.stimuli.world.Cube()
# Create a new visual scene that is presented for 5 seconds
vs = ptvr.stimuli.world.VisualScene(id=1, display=input.TimedDisplay(ms=5000))
# 'pos' stores position in Cartesian coordinates 
pos = np.array([0.0, 0.0, 0.0])
for i in range(1, 8):
	# Place 8 cubes at position 'pos'	
	vs.place(cube, pos)
	# Add a small offset to 'pos'
	pos = pos + np.array([0, 1, 0])
# Add scene to the experiment
exp.add_scene(vs)
\end{lstlisting}

%Screenshot of visual scene
\begin{figure}
    \centering
    \includegraphics[width=0.4\textwidth]{images/visual_scene}
    \caption{A \emph{visual scene} with several cubes}
    \label{fig:visual_scene}
\end{figure}

\subsubsection{Response scene}
A \emph{response scene} enables asking questions inside the virtual environment and recording user response to it.
This can, for example, be used for questions with objective multiple choice answers (for example- yes/no questions).
Though a \emph{response scene}'s functionality can be emulated by a \emph{visual scene} (a blank screen with just text and recording user input), this abstraction is easier to use for quick queries inside the virtual environment.
Code Snippet \ref{code_snippet:response_scene} and correspondingly, Figure \ref{fig:response_scene} show an example of a \emph{response scene}.

\begin{lstlisting}[caption=Creating a \emph{response scene}, label=code_snippet:response_scene]
# Add a response scene that asks a question and registers the response
exp.add_scene(ptvr.stimuli.world.ResponseScene(text="Did you see 7 cubes?\n[1]Yes\n[2]No"))
\end{lstlisting}

%Screenshot of response scene
\begin{figure}
    \centering
    \includegraphics[width=0.4\textwidth]{images/response_scene}
    \caption{\emph{Response scene} requesting participant input to a question}
    \label{fig:response_scene}
\end{figure}

\subsection{Abstractions for creating and placing objects}
Because of the underlying three-dimensional nature of VR environments, abstractions for creating and placing objects using \tool utilize 3D coordinates and shapes. 
Even a flat object such as a square or textbox is treated as a 3D object in the sense that it can be placed at any Cartesian coordinate \emph{(x,y,z)} and it is possible to look at it from any direction. 
This is unlike the case of 2D screens wherein objects have location \emph{(x,y)} and can only be looked at in the direction of the normal. 
In addition to placing objects at particular 3D locations, objects can also be rotated along all three axes. 
This enables placement of objects anywhere and in any orientation.
\tool also provides abstractions for texturing objects. 
For example, objects can be coloured or an image can be attached to them as per some 3D shading scheme.

\subsection{Timing and user input handling}
It is possible to have timed trials, that is, \emph{scene}s which are only presented for a particular period of time.
Due to limitations placed by refresh rates for most modern VR headsets, the timing precision is of the order of time a single frame is displayed. 
This is typically around 12 ms, depending on the VR display and computer hardware.

\tool also provides abstractions to record user input. 
It is difficult to use a standard mouse and keyboard while wearing a VR headset.
Most VR devices therefore utilize specialized input devices, typically handheld controllers.
Inputs from these controllers can be used as events from within the Python interface using key mappings as shown in Figure \ref{fig:key_mappings}.

% Hand controller key mappings
\begin{figure}
    \centering
    \includegraphics[width=0.4\textwidth]{images/key_mappings}
    \caption{Key mapping for HTC Vive VR hand controller (left and right hand controllers are mapped separately)}
    \label{fig:key_mappings}
\end{figure}

