"""
A simple example using the library
"""

import numpy as np
from ptvr import experiment
import ptvr.stimuli.world 
import ptvr.data.input as input


def main():

	# Set-up the experiment file.
    e = experiment.Experiment(name="My first experiment in VR", output_file="test.txt")

    fs = ptvr.stimuli.world.FixationScene(text="Hello World!")
    e.add_scene(fs)
	
    c = ptvr.stimuli.world.Cube()
    
	# A visual scene with a timeout of 5 seconds.
	vs = ptvr.stimuli.world.VisualScene(id=1, display=input.TimedDisplay(ms=5000))
    pos = np.array([0.0, 0.0, 0.0])
    for i in range(1, 8):
		# Place objects in the visual scene
        vs.place(c, pos, np.array([0.0, 0.0, 0.0]))
        pos = pos + np.array([1, 1, 1])
    # Add visual scene with the placed objects to the experiment.
	e.add_scene(ts)
	
	# Create a response scene to capture user response
    e.add_scene(ptvr.stimuli.world.ResponseScene(text="Did you see the cubes?\n[1]Yes\n[2]No"))

    # Finally, write the experiment (inclusive of added scenes to the output_file
    e.write()


if __name__ == "__main__":
    main()
