"""
Interfaces for textures. A texture is basically a 'skin' to a visual object and consists of a shader
and a corresponding color/image
"""

import ptvr.stimuli.color as color
 
class Texture(object):
    """
    A Texture class has three members- one, a shader that defines
    """
    def __init__(self, shader='diffuse', color=color.RGBColor(r=1.0), img=''):
        shaders = frozenset(['diffuse', 'unlit'])
        if shader in shaders:
            self.shader = shader # shader can be diffuse or unlit
        else:
            assert False, "Unknown shader %s (allowed shaders: %s)" % (shader, shaders)
        self.color = color
        self.img = img

    def to_json(self):
        return dict(shader=self.shader, color=self.color, img=self.img)
