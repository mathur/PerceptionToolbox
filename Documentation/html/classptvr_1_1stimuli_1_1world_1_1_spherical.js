var classptvr_1_1stimuli_1_1world_1_1_spherical =
[
    [ "__init__", "classptvr_1_1stimuli_1_1world_1_1_spherical.html#a815741d0e3b8eb24658c3080c4e375ae", null ],
    [ "degree2radian", "classptvr_1_1stimuli_1_1world_1_1_spherical.html#a761e9fa4d578b0394c98c680fa06848b", null ],
    [ "to_string", "classptvr_1_1stimuli_1_1world_1_1_spherical.html#a44cf831f99ec0f6d04868522ac810295", null ],
    [ "elevation", "classptvr_1_1stimuli_1_1world_1_1_spherical.html#aec46f04822b7fb71a82318ffecf4b61f", null ],
    [ "polar", "classptvr_1_1stimuli_1_1world_1_1_spherical.html#a8b4dd131425e2681c3455b4eac3a7015", null ],
    [ "r", "classptvr_1_1stimuli_1_1world_1_1_spherical.html#ac66af9ac96fad7cead814135ebd95c49", null ],
    [ "units", "classptvr_1_1stimuli_1_1world_1_1_spherical.html#ae8bbb93fd4e15bdddb7913ca42f73c47", null ]
];