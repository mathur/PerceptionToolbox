var classptvr_1_1stimuli_1_1color_1_1_r_g_b255_color =
[
    [ "__init__", "classptvr_1_1stimuli_1_1color_1_1_r_g_b255_color.html#a4fe7b505c3f2cf0e3dc3614e1541521d", null ],
    [ "__str__", "classptvr_1_1stimuli_1_1color_1_1_r_g_b255_color.html#a6e0ed139818d75c1bc0a210aeaf850c5", null ],
    [ "get_color", "classptvr_1_1stimuli_1_1color_1_1_r_g_b255_color.html#a4ae761f529bb0fc7a94c7190c0fe31d1", null ],
    [ "set_color", "classptvr_1_1stimuli_1_1color_1_1_r_g_b255_color.html#afd6e1f6876928d95c949d5c5a32cb3a9", null ],
    [ "to_json", "classptvr_1_1stimuli_1_1color_1_1_r_g_b255_color.html#a8662ee93c2df93172729045f2ef6b9be", null ],
    [ "a", "classptvr_1_1stimuli_1_1color_1_1_r_g_b255_color.html#a057f1438876b21dfaf3b2ec06ccdc69d", null ],
    [ "b", "classptvr_1_1stimuli_1_1color_1_1_r_g_b255_color.html#abe978a3fdf7f7b801fd55cb9c1827a8e", null ],
    [ "g", "classptvr_1_1stimuli_1_1color_1_1_r_g_b255_color.html#ad5e97522e8591c954737745a257cf3d6", null ],
    [ "r", "classptvr_1_1stimuli_1_1color_1_1_r_g_b255_color.html#a3798cd7ee922a8276e5bd7f1c23a500a", null ]
];