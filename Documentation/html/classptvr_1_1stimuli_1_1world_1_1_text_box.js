var classptvr_1_1stimuli_1_1world_1_1_text_box =
[
    [ "__init__", "classptvr_1_1stimuli_1_1world_1_1_text_box.html#ae0e8578c129f284abbcf77b5b3040be3", null ],
    [ "to_json", "classptvr_1_1stimuli_1_1world_1_1_text_box.html#a18121574c4212b8f1a7554174c815386", null ],
    [ "bold", "classptvr_1_1stimuli_1_1world_1_1_text_box.html#a0e4c7a7749946e5bacfaf469fa2c187d", null ],
    [ "font_color", "classptvr_1_1stimuli_1_1world_1_1_text_box.html#a6a56ea1109a5a03632e17fd3052f17b3", null ],
    [ "font_size", "classptvr_1_1stimuli_1_1world_1_1_text_box.html#aff27f2e25e3b0625a8edf5ede387bcf4", null ],
    [ "italic", "classptvr_1_1stimuli_1_1world_1_1_text_box.html#a6d5f3c58620e103c5a130e5de3e95a2e", null ],
    [ "text", "classptvr_1_1stimuli_1_1world_1_1_text_box.html#ad5964f1a788ee76e49251a91a938bc94", null ]
];