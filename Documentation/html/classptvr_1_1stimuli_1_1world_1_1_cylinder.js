var classptvr_1_1stimuli_1_1world_1_1_cylinder =
[
    [ "__init__", "classptvr_1_1stimuli_1_1world_1_1_cylinder.html#aaed6edde0e83117fb9168a43617bc85a", null ],
    [ "to_json", "classptvr_1_1stimuli_1_1world_1_1_cylinder.html#ab2b005306319345a47f9b743297687ee", null ],
    [ "color", "classptvr_1_1stimuli_1_1world_1_1_cylinder.html#aee2ee0ab491a31e2e721f74d09c1b8b2", null ],
    [ "height", "classptvr_1_1stimuli_1_1world_1_1_cylinder.html#a45edee0bde0b9d381dfd9eee925918f9", null ],
    [ "radius", "classptvr_1_1stimuli_1_1world_1_1_cylinder.html#a8145f795c64f3cdf5702df9d14e453ba", null ],
    [ "texture", "classptvr_1_1stimuli_1_1world_1_1_cylinder.html#a394a83bf9ecefd7ab3349e48a405a1d7", null ]
];