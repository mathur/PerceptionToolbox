var color_8py =
[
    [ "Color", "classptvr_1_1stimuli_1_1color_1_1_color.html", "classptvr_1_1stimuli_1_1color_1_1_color" ],
    [ "RGBColor", "classptvr_1_1stimuli_1_1color_1_1_r_g_b_color.html", "classptvr_1_1stimuli_1_1color_1_1_r_g_b_color" ],
    [ "RGB255Color", "classptvr_1_1stimuli_1_1color_1_1_r_g_b255_color.html", "classptvr_1_1stimuli_1_1color_1_1_r_g_b255_color" ],
    [ "HexColor", "classptvr_1_1stimuli_1_1color_1_1_hex_color.html", "classptvr_1_1stimuli_1_1color_1_1_hex_color" ],
    [ "hextorgb255", "color_8py.html#a323309b8f69f6cb4be03a484b8b8f1de", null ],
    [ "rgb255tohex", "color_8py.html#a5d1bfbf4e48a5fc4b4db447caf3c0c66", null ],
    [ "colorsHex", "color_8py.html#ae846c0d26f2e2bddf74e56a1d0f55ac5", null ],
    [ "colorsRGB", "color_8py.html#afb61b9f6b41301f8a911f3cf39728aee", null ],
    [ "colorsRGB255", "color_8py.html#a70bcbad40ec8afc0c49abf0f01726f14", null ]
];