var classptvr_1_1stimuli_1_1color_1_1_r_g_b_color =
[
    [ "__init__", "classptvr_1_1stimuli_1_1color_1_1_r_g_b_color.html#af86e40f446a393bb2c5488a39950ea09", null ],
    [ "__str__", "classptvr_1_1stimuli_1_1color_1_1_r_g_b_color.html#ac4d6151da808e1500a8420ad745f2854", null ],
    [ "get_color", "classptvr_1_1stimuli_1_1color_1_1_r_g_b_color.html#ac47abba9889616f7c878bda614c260e0", null ],
    [ "set_color", "classptvr_1_1stimuli_1_1color_1_1_r_g_b_color.html#af5fc16cde8db06ca86587dfe16a17cb2", null ],
    [ "to_json", "classptvr_1_1stimuli_1_1color_1_1_r_g_b_color.html#a938870d1132252d9be07b02b031665d0", null ],
    [ "a", "classptvr_1_1stimuli_1_1color_1_1_r_g_b_color.html#ab04895184d5ddf974fed9ae4d68e7b51", null ],
    [ "b", "classptvr_1_1stimuli_1_1color_1_1_r_g_b_color.html#ade5716b1fc1716845047c262fcdad5c2", null ],
    [ "g", "classptvr_1_1stimuli_1_1color_1_1_r_g_b_color.html#ab470ba95f69014ea7364fc875d8052e7", null ],
    [ "r", "classptvr_1_1stimuli_1_1color_1_1_r_g_b_color.html#a0cf01c4807190987fa10018469b6c2f0", null ]
];