var searchData=
[
  ['g',['g',['../classptvr_1_1stimuli_1_1color_1_1_r_g_b_color.html#ab470ba95f69014ea7364fc875d8052e7',1,'ptvr.stimuli.color.RGBColor.g()'],['../classptvr_1_1stimuli_1_1color_1_1_r_g_b255_color.html#ad5e97522e8591c954737745a257cf3d6',1,'ptvr.stimuli.color.RGB255Color.g()']]],
  ['get',['get',['../classptvr_1_1data_1_1input_1_1_display.html#aa6d494fee8ca6db00d48fd68088e1e7c',1,'ptvr::data::input::Display']]],
  ['get_5fcolor',['get_color',['../classptvr_1_1stimuli_1_1color_1_1_r_g_b_color.html#ac47abba9889616f7c878bda614c260e0',1,'ptvr.stimuli.color.RGBColor.get_color()'],['../classptvr_1_1stimuli_1_1color_1_1_r_g_b255_color.html#a4ae761f529bb0fc7a94c7190c0fe31d1',1,'ptvr.stimuli.color.RGB255Color.get_color()'],['../classptvr_1_1stimuli_1_1color_1_1_hex_color.html#a11df53b8b52214ea60362802d1e9cd64',1,'ptvr.stimuli.color.HexColor.get_color()']]],
  ['get_5fevent',['get_event',['../classptvr_1_1data_1_1input_1_1_keyboard_event.html#a1ba82e8f8ac7fe1f3a2ac87e195b6d9c',1,'ptvr::data::input::KeyboardEvent']]]
];
