var searchData=
[
  ['r',['r',['../classptvr_1_1stimuli_1_1color_1_1_r_g_b_color.html#a0cf01c4807190987fa10018469b6c2f0',1,'ptvr.stimuli.color.RGBColor.r()'],['../classptvr_1_1stimuli_1_1color_1_1_r_g_b255_color.html#a3798cd7ee922a8276e5bd7f1c23a500a',1,'ptvr.stimuli.color.RGB255Color.r()'],['../classptvr_1_1stimuli_1_1world_1_1_spherical.html#ac66af9ac96fad7cead814135ebd95c49',1,'ptvr.stimuli.world.Spherical.r()']]],
  ['radius',['radius',['../classptvr_1_1stimuli_1_1world_1_1_sphere.html#af761eba64ce761253687ae80558f0558',1,'ptvr.stimuli.world.Sphere.radius()'],['../classptvr_1_1stimuli_1_1world_1_1_cylinder.html#a8145f795c64f3cdf5702df9d14e453ba',1,'ptvr.stimuli.world.Cylinder.radius()']]],
  ['results',['results',['../classptvr_1_1data_1_1trial_1_1_trial.html#ab49bdb50ceb21b319d9c1fceef692fde',1,'ptvr::data::trial::Trial']]],
  ['rotation',['rotation',['../classptvr_1_1stimuli_1_1world_1_1_visual_object.html#ad7bf9b37edd866d1258329e043bcf177',1,'ptvr::stimuli::world::VisualObject']]],
  ['run',['run',['../classptvr_1_1data_1_1trial_1_1_trial.html#a1bd19c8220d260a77d6313b0533d426a',1,'ptvr::data::trial::Trial']]]
];
