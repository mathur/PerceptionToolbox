var searchData=
[
  ['text',['text',['../classptvr_1_1stimuli_1_1world_1_1_text_box.html#ad5964f1a788ee76e49251a91a938bc94',1,'ptvr.stimuli.world.TextBox.text()'],['../classptvr_1_1stimuli_1_1world_1_1_fixation_scene.html#aa2d65b2e0469e41d088a11b6e56e0ea9',1,'ptvr.stimuli.world.FixationScene.text()'],['../classptvr_1_1stimuli_1_1world_1_1_response_scene.html#a3fc347caa3a0161d04a89959c69745f0',1,'ptvr.stimuli.world.ResponseScene.text()']]],
  ['texture',['texture',['../classptvr_1_1stimuli_1_1world_1_1_cube.html#a58d06e2e14ac6350d1091b5cacf287b1',1,'ptvr.stimuli.world.Cube.texture()'],['../classptvr_1_1stimuli_1_1world_1_1_sphere.html#afd46905279a9e11ab5c5eeb2d84a5e2e',1,'ptvr.stimuli.world.Sphere.texture()'],['../classptvr_1_1stimuli_1_1world_1_1_cylinder.html#a394a83bf9ecefd7ab3349e48a405a1d7',1,'ptvr.stimuli.world.Cylinder.texture()'],['../classptvr_1_1stimuli_1_1world_1_1_quad.html#a6370fe0323a3a741f8e1eb195d9b522a',1,'ptvr.stimuli.world.Quad.texture()']]],
  ['twopi',['TWOPI',['../namespaceptvr_1_1stimuli_1_1world.html#aacb49bf4f2014054d9bf12cecfdf4004',1,'ptvr::stimuli::world']]]
];
