var indexSectionsWithContent =
{
  0: "_abcdefghijklmnopqrstuvwxyz",
  1: "cdefhijkmqrstuv",
  2: "p",
  3: "_ceistw",
  4: "_acdeghmoprstw",
  5: "abcdefghilmnoprstuvxyz",
  6: "c"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "properties"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Properties"
};

