var searchData=
[
  ['set_5fcolor',['set_color',['../classptvr_1_1stimuli_1_1color_1_1_r_g_b_color.html#af5fc16cde8db06ca86587dfe16a17cb2',1,'ptvr.stimuli.color.RGBColor.set_color()'],['../classptvr_1_1stimuli_1_1color_1_1_r_g_b255_color.html#afd6e1f6876928d95c949d5c5a32cb3a9',1,'ptvr.stimuli.color.RGB255Color.set_color()'],['../classptvr_1_1stimuli_1_1color_1_1_hex_color.html#aa8b5036f407d08770df3315b25777208',1,'ptvr.stimuli.color.HexColor.set_color()']]],
  ['show',['show',['../classptvr_1_1data_1_1input_1_1_display.html#ac2987e5e0c515e15ec6afd4530aa5b9b',1,'ptvr::data::input::Display']]],
  ['spherical2cartesian',['spherical2cartesian',['../namespaceptvr_1_1stimuli_1_1world.html#aecf16e21011ededf8a7c9291ffd774ef',1,'ptvr::stimuli::world']]]
];
