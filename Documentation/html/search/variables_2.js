var searchData=
[
  ['color',['color',['../classptvr_1_1stimuli_1_1texture_1_1_texture.html#a798f26c38854554b657d27fa3eed273b',1,'ptvr.stimuli.texture.Texture.color()'],['../classptvr_1_1stimuli_1_1world_1_1_cube.html#a4ae750e50dc5648f4a3a1c5ffa4181ef',1,'ptvr.stimuli.world.Cube.color()'],['../classptvr_1_1stimuli_1_1world_1_1_sphere.html#a0a35cb7f123d664e142b682f2f53322e',1,'ptvr.stimuli.world.Sphere.color()'],['../classptvr_1_1stimuli_1_1world_1_1_cylinder.html#aee2ee0ab491a31e2e721f74d09c1b8b2',1,'ptvr.stimuli.world.Cylinder.color()'],['../classptvr_1_1stimuli_1_1world_1_1_quad.html#a3f033089bdf40c0f7652b7d264182021',1,'ptvr.stimuli.world.Quad.color()']]],
  ['colorshex',['colorsHex',['../namespaceptvr_1_1stimuli_1_1color.html#ae846c0d26f2e2bddf74e56a1d0f55ac5',1,'ptvr::stimuli::color']]],
  ['colorsrgb',['colorsRGB',['../namespaceptvr_1_1stimuli_1_1color.html#afb61b9f6b41301f8a911f3cf39728aee',1,'ptvr::stimuli::color']]],
  ['colorsrgb255',['colorsRGB255',['../namespaceptvr_1_1stimuli_1_1color.html#a70bcbad40ec8afc0c49abf0f01726f14',1,'ptvr::stimuli::color']]],
  ['created',['created',['../classptvr_1_1data_1_1trial_1_1_trial.html#a3c3335a5cb96ac503da377d00c545b81',1,'ptvr.data.trial.Trial.created()'],['../classptvr_1_1experiment_1_1_experiment.html#aa283527f1255ebe500167b91d44396be',1,'ptvr.experiment.Experiment.created()']]]
];
