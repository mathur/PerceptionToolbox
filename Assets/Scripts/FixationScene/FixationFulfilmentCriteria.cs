﻿//Fulfilment criteria for fixation
//Checks if the user is actually looking at the fixation screen and standing on the 2 stripes on the floor

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FixationFulfilmentCriteria : MonoBehaviour {
    public Transform Placeholder;
    public Transform Camera;

    private bool valid = false;
    //public static float height;
    private int layerMaskForPlane;
    
    // Use this for initialization
    void Start()
    {
        layerMaskForPlane = LayerMask.GetMask("Fixation"); //Only hit the laer of Fixation GameObjects
    }


    void FixedUpdate()
    {
        Ray rayF = new Ray(Camera.transform.position, Camera.transform.forward); //forward
        RaycastHit hitF;

        Ray rayD = new Ray(Camera.transform.position, -Camera.transform.up); //downward
        RaycastHit hitD;

        if (Physics.Raycast(rayF, out hitF, 8, layerMaskForPlane) && Physics.Raycast(rayD, out hitD, 4, layerMaskForPlane)) //The plane can be max 8 / 4 metres away
        {
            valid = true;
            //height = Camera.transform.position.y;
        }
        else
            valid = false;

    }

    public void Fulfilled()
    {
        if (valid) //Looking at fixation plane, standing in the correct position and grip clicked
        {
            Debug.Log("Button clicked and looking at fixation-> Next step");
            Placeholder.GetComponent<VisualManager>().UnputFixation(); //No need of status check because fixation objects are otherwise disabled
        }
        else
            Debug.Log("Button clicked and not looking at fixation-> Unacceptable");
    }

    public void StartTimer()
    {
        Debug.LogError("Timer for Fixation scene not implemented");
    }
}
