﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetFixationSettings : MonoBehaviour {

    public Transform textfield;

    public void Set(ptvr.stimuli.world.FixationScene fs)
    {
        textfield.GetComponent<TextMesh>().text = fs.text;
    }
}
