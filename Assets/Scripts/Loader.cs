﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using Newtonsoft.Json;

public enum SystemState
{
    INITIALIZED,
    FIXATION_NOW,
    VISUAL_NOW,
    RESPONSE_NOW
};

public class Loader : MonoBehaviour {

    private ptvr.experiment currExperiment; //List of scenes
    public SystemState state = SystemState.INITIALIZED;

    //Executed before Start()
    private void Awake()
    {
        StreamReader reader; //Reads the experiment description
        string filename = "Python/samples/expLeftVRight.txt";
        string userId = "DummyUser";
        //--------------------------------------------------------------------------
        //TODO: MOdify this to run automatically from ptvr's Python run call.
        //--------------------------------------------------------------------------
        if (System.Environment.GetCommandLineArgs().Length == 3)
        {
            userId = System.Environment.GetCommandLineArgs()[2];
            filename = System.Environment.GetCommandLineArgs()[1];
        }
        else if (System.Environment.GetCommandLineArgs().Length == 2)
        {
            filename = System.Environment.GetCommandLineArgs()[1];
        }
        else
        {
            Debug.Log("The program expects 2 arguments--> exp.exe <experiment_file.txt> <user_id>" +
                "\nUsing Defaults...");
        }
        reader = new StreamReader(filename);

        currExperiment = new ptvr.experiment(); //The experiment structure is initialized

        //Read the file
        string readData = reader.ReadToEnd(); //CAUTION: MAY BE A CAUSE OF WORRY FOR VERY LARGE FILE SIZES

        Debug.Log("The experiment description has been read. ");

        //TODO: Add additional converters to support polymorphism
        var settings = new JsonSerializerSettings();
        settings.Converters.Add(new ptvr.Converters.SceneConverter());
        settings.Converters.Add(new ptvr.Converters.VisualObjectTypeConverter());
        settings.Converters.Add(new ptvr.Converters.ColorFormatConverter());
        settings.Converters.Add(new ptvr.Converters.DisplayConverter());
        settings.Converters.Add(new ptvr.Converters.InputEventConverter());

        currExperiment = JsonConvert.DeserializeObject<ptvr.experiment>(readData, settings);

        //We set the result file name
        string resultFileName = currExperiment.name + "_" + userId + "_" + System.DateTime.Now.ToString("yyyy-dd-M--HH-mm-ss");
        GetComponent<ResultManager>().SetupFile(resultFileName);
        
    }

    private void Start()
    {
        LoadNext(); //Start loading things
    }

    private void LoadNext() //Called from within this script only
    //Loads the next scene to be displayed
    {
        if (currExperiment.scenes.Count > 0) //The scenes list contains info on what scenes to show, in what order
        {
            //What type of scene is the top-most scene
            switch (currExperiment.scenes[0].type) 
            {
                case "FIXATION_SCENE":
                    Debug.Log("Changing state to FIXATION_SCENE");
                    state = SystemState.FIXATION_NOW;
                    GetComponent<VisualManager>().PutFixation((ptvr.stimuli.world.FixationScene)currExperiment.scenes[0]);
                    //Set GenericFulfilmentCriteria
                    break;
                case "VISUAL_SCENE":
                    Debug.Log("Changing state to VISUAL_SCENE");
                    state = SystemState.VISUAL_NOW;
                    GetComponent<VisualManager>().PutVisual((ptvr.stimuli.world.VisualScene)currExperiment.scenes[0]);
                    break;
                case "RESPONSE_SCENE":
                    Debug.Log("Changing state to RESPONSE_SCENE");
                    state = SystemState.RESPONSE_NOW;
                    GetComponent<VisualManager>().PutPost((ptvr.stimuli.world.ResponseScene)currExperiment.scenes[0]);
                    break;
                default:
                    Debug.LogError("Unknown scene by scene type encountered");
                    break;
            }
            //
        }
        else //Experiment complete
        {
            Debug.Log("Experiment finished");
        }
    }

    public void CurrentFinished()
    {
        if (currExperiment.scenes.Count > 0)
        {
            currExperiment.scenes.RemoveAt(0); //Remove current object at the top of the list
        }
        LoadNext(); //Load next object
    }

}
