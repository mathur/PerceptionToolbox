﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VisualManager : MonoBehaviour
{

    public Transform FixationTemplate; //Template of the fixation screen

    public Transform TrialParent;

    public Transform PostTemplate;

    //-----------------FIXATION------------------------
    public void PutFixation (ptvr.stimuli.world.FixationScene fs)
    {
        FixationTemplate.gameObject.SetActive(true);
        FixationTemplate.GetComponent<SetFixationSettings>().Set(fs);

        FixationTemplate.GetComponent<GenericFulfilmentCriteria>().SetFulfilmentVariables(fs.display);
    }
    public void UnputFixation()
    {
        FixationTemplate.gameObject.SetActive(false);
        FinishThis();
    }

    //-----------------TRIAL (Renamed Visual Scene)------------------------
    public void PutVisual (ptvr.stimuli.world.VisualScene ts)
    {
        TrialParent.GetComponent<SetTrialConfiguration>().SetScene(ts);
        
        //Set generic fulfilment criteria
        TrialParent.GetComponent<GenericFulfilmentCriteria>().SetFulfilmentVariables(ts.display);
    }

    public void UnputTrial()
    {
        TrialParent.GetComponent<GenericFulfilmentCriteria>().UnSetFulfilmentVariables();
        //Objects part of the trial have already been deleted 
        FinishThis();
    }

    //-----------------POST------------------------
    //CAUTION: Renamed to Response
    public void PutPost (ptvr.stimuli.world.ResponseScene ps)
    {
        PostTemplate.gameObject.SetActive(true);
        PostTemplate.GetComponent<SetPostSettings>().Set(ps);
        PostTemplate.GetComponent<GenericFulfilmentCriteria>().SetFulfilmentVariables(ps.display);
    }
    public void UnputPost()
    {
        PostTemplate.gameObject.SetActive(false);
        FinishThis();
    }

    public void FinishThis()
    {
        GetComponent<Loader>().CurrentFinished();
    }


}
