﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetPostSettings : MonoBehaviour {

    public Transform textfield;

    public void Set(ptvr.stimuli.world.ResponseScene rs)
    {
        GetComponent<PostFulfilmentCriteria>().SetId(rs.id);
        textfield.GetComponent<TextMesh>().text = rs.text;
    }
}
