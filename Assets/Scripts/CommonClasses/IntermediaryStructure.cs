﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
//Class for storing texture of objects (ObjectIn3D)
public class ObjectTexture
{
    public string shader;
    public Color color;
    public string imgPath;
}

[System.Serializable]
public class ObjectIn3D
{
    public string type;
    public Vector3 position, rotation, scale;
    public ObjectTexture texture;
}

[System.Serializable]
//Base class for all intermediary structures
public class BaseStructure
{
    public string baseType; //GLOBAL_SETTING, SCENE_BY_SCENE_SETTING, etc
}

//Consists of data about global settings
public class GlobalSettings : BaseStructure
{
    public string resultFormat;
    public bool whiteBg; //Color scheme
    public bool fixedOrigin; //Do things stay fixed or move with the origin?
}

//Consists of data about scene-by-scene settings
public class SceneByScene : BaseStructure
{
    public string typeOfScene;
}

//Specifics about the fixation screen
public class FixationScene : SceneByScene
{
    public string text;
}

//The trial-- timed or user directed, has many 3d objects in the environment
public class TrialScene : SceneByScene
{
    public float timeForTrial;
    public List<ObjectIn3D> objectsInScene;
}

//Consists of a question (text) with two options
public class PostScene : SceneByScene
{
    public string question, option1, option2;
}





