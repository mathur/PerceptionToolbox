﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalSettingsSetter : MonoBehaviour {

    public Transform Camera; //To set background colour + To re-do parenting
    public Transform [] TextFields;
    public Transform PostTemplate; 
    public Material White;
    public Material Black;

    public Transform TrialTemplate;


    public void SetSettings(GlobalSettings gs)
    {
        //Result format
        switch(gs.resultFormat.ToUpper())
        {
            default:
                Debug.LogError("Currently the only possible reporting is automatically implemented");
                break;
        }

        //Set the color scheme
        if (!gs.whiteBg)
        {
            //Black
            Camera.GetComponent<Camera>().backgroundColor = Color.black;
            
            foreach (Transform textField in TextFields)
            {
                textField.GetComponent<TextColorFlipper>().ColorForBlackBg();
            }

            //Flip color of materials used
            White.color = Color.black;
            Black.color = Color.white;
        }
        else
        {
            Camera.GetComponent<Camera>().backgroundColor = Color.white;

            foreach (Transform textField in TextFields)
            {
                textField.GetComponent<TextColorFlipper>().ColorForWhiteBg();
            }

            White.color = Color.white;
            Black.color = Color.black;
        }

        //Set fixed/moving origin
        if(!gs.fixedOrigin)
        {
            TrialTemplate.parent = Camera;
            //FixationTemplate.parent = Camera;
            PostTemplate.parent = Camera;
        }

    }
}
