﻿/*
 *This script manages the end of a Trial Scene 
 */


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrialFulfilmentCriteria : MonoBehaviour {

    public Transform Placeholder;

    //------------------Timing realted variables-------------------------------
    private float timeAtStart = 0f; //Set at the beginning, see StartTimer()
    private float timeCurrent = 0f; //Updated each frame, see LateUpdate()

    public float timeForTrial = 0f; 
    public float reactionTime = 0f;
    //------------------Timing realted variables-------------------------------

    public int id=-1; //Identifier of the scene in experiment structure
            
    void LateUpdate () {
        if (Placeholder.GetComponent<Loader>().state == SystemState.VISUAL_NOW)
        {
            timeCurrent = Time.realtimeSinceStartup; //Re-set current time

            reactionTime = timeCurrent - timeAtStart; //Get the reaction time (so far)
            
            if(reactionTime >= timeForTrial && timeForTrial > 0f)
            {
                Fulfilled("NoControllerEvent");
            }
        }

    }

    public void SetId(int n)
    {
        id = n;
    }

    public void SetTimeForTrial(float timeInSeconds) //Set the terminationtime for the scene
    {
        timeForTrial = timeInSeconds;
    }

    /*
    public void TriggerClicked()
    {
        if (Placeholder.GetComponent<Loader>().state == SystemState.VISUAL_NOW && timeForTrial == 0f)
                                                                           //It was not a timed stimulus         
        {
            Fulfilled();
        }
    }
    */

    //Prepares the list of things to write in the result
    public List<string> GetResult(string button)
    {
        List<string> result = new List<string>();
        //Add things to the result list
        result.Add(id.ToString()); // Add the identifier to the scene
        result.Add(reactionTime.ToString()); // Reaction time
        result.Add(button); // Button pressed
        return result;
    }

    public void StartTimer() //Called via SendMessage call from GenericFulfilmentCriteria
    {
        timeAtStart = Time.realtimeSinceStartup; //Set Reaction Time at the beginning of a trial

        //Reset variables
        timeCurrent = 0f; 
        reactionTime = 0f;
    }

    public void Fulfilled (string button)
    {
        if (Placeholder.GetComponent<Loader>().state == SystemState.VISUAL_NOW)
        {
            foreach (Transform child in transform) //Delete all gameobjects in the visual scene
                Destroy(child.gameObject);
            
            if (button != "NoControllerEvent")
                Debug.Log("Trial ending as " + button + " was pressed");
            else
                Debug.Log("Trial ending because of a timeout");
            List<string> result = GetResult(button);
            Placeholder.GetComponent<ResultManager>().WriteToFile(result);
            id = -1; //Reset the id

            Placeholder.GetComponent<VisualManager>().UnputTrial();
        }
    }
}
