﻿//Objects in the Trial are created (and destroyed) under the parent

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetTrialConfiguration : MonoBehaviour {

    public GameObject[] BasicObjects;

    public Transform Placeholder;
    public Transform Camera; //A reference to the camera game object  
    
    //Shaders are set in the inspector
    public Shader diffuse;
    public Shader unlit;


    //This prefix is required for Windows for local drive retrieval
    private string prefixForWWWQuery = "file:///";
    

    public void SetScene(ptvr.stimuli.world.VisualScene ts)
    {
        //GetComponent<TrialFulfilmentCriteria>().timeForTrial = ts.timeForTrial; //Set the time we need this trial for

        GetComponent<TrialFulfilmentCriteria>().SetId(ts.id); //Set the id. It is used during fulfilment to store results with ids.

        Camera.GetComponent<Camera>().backgroundColor = ts.background_color.toUnityColor(); //Set the background colour

        foreach (ptvr.stimuli.world.PlacedObject po in ts.objects)
        {
            GameObject go = CreateObject(po.obj);

            //Set position and orientation
            go.transform.position = po.toUnityPosition();
            //Set rotation (we add rotation to initial because of initials set in prefab)
            go.transform.rotation = Quaternion.Euler(po.toUnityRotation() + go.transform.rotation.eulerAngles); 
        }

        //Print time since start-up
        //Debug.Log("Real time at start: " + Time.realtimeSinceStartup.ToString());
    }

    //Instantiate objects in virtual space
    private GameObject CreateObject(ptvr.stimuli.world.VisualObject obj) 
    {
        GameObject go = new GameObject();
        switch (obj.type) //Create proper mesh and set correct size
        {
            case "cube":
                ptvr.stimuli.world.Cube c = (ptvr.stimuli.world.Cube)obj;
                go = Instantiate(BasicObjects[1]);
                go.transform.localScale = c.toUnityScale(); //Set size

                if (c.color.toUnityColor() == new Color(0f, 0f, 0f, 0f))
                    go.GetComponent<Renderer>().material = CreateMaterial(c.texture); //Set material
                else
                    go.GetComponent<Renderer>().material.color = c.color.toUnityColor();
                break;
            case "quad":
                ptvr.stimuli.world.Quad q = (ptvr.stimuli.world.Quad)obj;
                go = Instantiate(BasicObjects[4]);
                go.transform.localScale = q.toUnityScale(); //Set size
                if (q.color.toUnityColor() == new Color(0f, 0f, 0f, 0f))
                    go.GetComponent<Renderer>().material = CreateMaterial(q.texture); //Set material
                else
                    go.GetComponent<Renderer>().material.color = q.color.toUnityColor();
                break;
            case "sphere":
                ptvr.stimuli.world.Sphere s = (ptvr.stimuli.world.Sphere)obj;
                go = Instantiate(BasicObjects[5]);
                go.transform.localScale = s.toUnityScale(); //Set size
                if (s.color.toUnityColor() == new Color(0f, 0f, 0f, 0f))
                    go.GetComponent<Renderer>().material = CreateMaterial(s.texture); //Set material
                else
                    go.GetComponent<Renderer>().material.color = s.color.toUnityColor();
                break;
            case "cylinder":
                ptvr.stimuli.world.Cylinder cyl = (ptvr.stimuli.world.Cylinder)obj;
                go = Instantiate(BasicObjects[2]);
                go.transform.localScale = cyl.toUnityScale(); //Set size
                if (cyl.color.toUnityColor() == new Color(0f, 0f, 0f, 0f))
                    go.GetComponent<Renderer>().material = CreateMaterial(cyl.texture); //Set material
                else
                    go.GetComponent<Renderer>().material.color = cyl.color.toUnityColor();
                break;
            case "text_box":
                ptvr.stimuli.world.TextBox tb = (ptvr.stimuli.world.TextBox)obj;
                go = Instantiate(BasicObjects[6]);
                //The scale is not set here to preserve font resolution
                TextMesh textmesh = go.GetComponent<TextMesh>();
                //Set text box sspecific properties
                textmesh.color = tb.font_color.toUnityColor();
                textmesh.fontSize = tb.font_size;
                //Set style
                if (tb.bold && tb.italic)
                    textmesh.fontStyle = FontStyle.BoldAndItalic;
                else if (tb.bold)
                    textmesh.fontStyle = FontStyle.Bold;
                else if (tb.italic)
                    textmesh.fontStyle = FontStyle.Italic;
                break;
            default:
                Debug.LogError("Unknown VisualObject type encountered.");
                break;
        }

        go.transform.parent = transform; //Set parent as the Trial parent 

        return go;
    }

    //Use the texture data member in ptvr library and unroll it into a Unity material that can be easily set
    private Material CreateMaterial(ptvr.stimuli.Texture t)
    {
        Material mat;
        switch (t.shader) //Set proper shader
        {
            case "diffuse":
                mat = new Material(diffuse);
                break;
            case "unlit":
                mat = new Material(unlit);
                break;
            default:
                mat = new Material(diffuse);
                Debug.Log("Unknown shader encountered. Defaulting to Diffuse.");
                break;
        }

        mat.color = t.color.toUnityColor(); //Set proper color

        if (t.img.Length > 0) //If an image texture needs to be put
        {
            WWW imgPathRetriever = new WWW(prefixForWWWQuery + t.img);
            mat.mainTexture = imgPathRetriever.texture;
        }

        return mat;
    }

}
