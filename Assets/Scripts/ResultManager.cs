﻿// Holds functions that create, add to and close the result file

using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class ResultManager : MonoBehaviour {

    private StreamWriter resultWriter;
    private string resultFileName = "null";

    public void SetupFile(string str)
    { // Setup the result file by creating and opening it
        resultFileName = str + ".csv";

        resultWriter = new StreamWriter(resultFileName);

        resultWriter.AutoFlush = true; //No effect on performance

        resultWriter.WriteLine("SceneIdentifier, ReactionTime, UserInput, Timestamp"); //Add a header
    }

    public void WriteToFile(List<string> data) //Writes a list of strings in a specific format
    {
        foreach (var item in data)
        {
            resultWriter.Write(item + ", " );
        }

        resultWriter.Write(System.DateTime.Now.ToString("yyyy-mm-dd--HH-mm-ss.fff") + "\n");
    }

    private void OnApplicationQuit()
    { // Close this file once the application quits
        resultWriter.Close(); 
    }

}