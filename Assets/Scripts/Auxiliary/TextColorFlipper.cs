﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextColorFlipper : MonoBehaviour
{
    public void ColorForBlackBg()
    {
        GetComponent<TextMesh>().color = Color.white;
    }

    public void ColorForWhiteBg()
    {
        GetComponent<TextMesh>().color = Color.black;
    }
}
