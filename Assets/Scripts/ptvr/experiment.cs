﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ptvr //Toolbox namespace
{
    public class experiment
    {
        public string name; //Name of the experiment

        public string created; //date-time when created

        public string output_file;

        public IList<ptvr.stimuli.world.Scene> scenes; //List of scenes in the experiment
    }
}
