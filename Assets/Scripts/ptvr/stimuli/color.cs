﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ptvr
{
    namespace stimuli
    {
        [System.Serializable]
        public abstract class Color
        {
            public string type; //The type of Color specification 

            public abstract UnityEngine.Color toUnityColor();
        }

        public class RGBColor : Color //Similar to the default Unity encoding
        {
            public float r, g, b, a;

            public override UnityEngine.Color toUnityColor()
            {
                return new UnityEngine.Color(r, g, b, a);
            }
        }

        public class HexColor : Color
        {
            public string color; //hexadecimal value
            public float a;
            public override UnityEngine.Color toUnityColor()
            {
                string colorString = color.ToString();
                colorString = "#" + colorString; //Convert to string and add a '#' to indicate hex value
                UnityEngine.Color col;
                ColorUtility.TryParseHtmlString(colorString, out col);
                col.a = a;
                return col;
            }
        }

        public class RGB255Color : Color
        {
            public int r,g,b; //values 0 to 255
            public float a;

            public override UnityEngine.Color toUnityColor()
            {
                return new UnityEngine.Color(r/255f, g/255f, b/255f, a);
            }
        }
    }
}
