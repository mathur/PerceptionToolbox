﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ptvr
{
    namespace stimuli
    {
        public class Texture
        {
            public string shader;
            public ptvr.stimuli.Color color;
            public string img;
            //No method to convert to Unity Standard---> Unrolling done in SetTrialConfiguration.cs
        }
    }
}
