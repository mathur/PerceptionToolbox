﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ptvr
{
    namespace stimuli
    {
        namespace world
        {
            public abstract class Frame
            {
                public abstract Vector3 toUnityPos();
            }

            public class Cartesian : Frame
            {
                public float x, y, z;
                public override Vector3 toUnityPos()
                {
                    return new Vector3(x, y, z);
                }
            }

            public class PlacedObject
            {
                //Position and orientation, in addition to object reference
                public List<float> position;
                public List<float> orientation;
                public VisualObject obj;

                public Vector3 toUnityPosition()
                {
                    return new Vector3(position[0], position[1], position[2]);
                }

                public Vector3 toUnityRotation()
                {
                    return new Vector3(orientation[0], orientation[1], orientation[2]);
                }
            }


            public class VisualObject
            {
                public string type; //The type of object, eg- cube, sphere, text_box etc.
            }


            //VisualObjectDecorator not implemented

            public class TextBox : VisualObject
            {
                public string name, text, font_name, align_horizontal, align_vertical;
                public ptvr.stimuli.Color font_color; 
                public bool bold, italic;
                public int font_size;
            }

            public class Cube : VisualObject
            {
                public float side;
                public ptvr.stimuli.Color color; //CAUTION: This data member is ignored as Color is repeated inside texture
                public ptvr.stimuli.Texture texture;

                public Vector3 toUnityScale()
                {
                    return new Vector3(side, side, side);
                }
            }

            public class Sphere : VisualObject
            {
                public float radius;
                public ptvr.stimuli.Color color;
                public ptvr.stimuli.Texture texture;

                public Vector3 toUnityScale()
                {
                    return new Vector3(radius / 2f, radius / 2f, radius / 2f);
                }
            }

            public class Cylinder : VisualObject
            {
                public float radius;
                public float height;
                public ptvr.stimuli.Color color;
                public ptvr.stimuli.Texture texture;

                public Vector3 toUnityScale()
                {
                    return new Vector3(radius / 2f, height, radius / 2f);
                }
            }

            public class Quad : VisualObject
            {
                public float side;
                public ptvr.stimuli.Color color; //CAUTION: This data member is ignored as Color is repeated inside texture
                public ptvr.stimuli.Texture texture;

                public Vector3 toUnityScale()
                {
                    return new Vector3(side, side, side);
                }
            }


            //------------------Scenes------------------------------------------

            public class Scene
            {
                public int id; //A (not-neseccarily) unique identifier for the scene
                public string type;
                public ptvr.data.input.Display display; //Holds information on termination criteria and user inputs possible
            }

            public class FixationScene : Scene
            {
                public string text;
            }

            public class PostScene : Scene  //May be removed in the future
            {
                public string question, option1, option2;
            }

            public class VisualScene : Scene
            {
                public ptvr.stimuli.Color background_color;
                public List<PlacedObject> objects; //Objects in the visual scene
            }

            public class ResponseScene : Scene  //User response screen
            {
                public string text;
            }

        }
    }
}
