﻿//Helps converting between various color formats
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace ptvr
{
    namespace Converters
    {
        //Inspired from example at https://blog.codeinside.eu/2015/03/30/json-dotnet-deserialize-to-abstract-class-or-interface/public 
        public class ColorFormatConverter : JsonConverter
        {
            public override bool CanConvert(System.Type objectType)
            {
                return (objectType == typeof(ptvr.stimuli.Color));
            }

            public override object ReadJson(JsonReader reader, System.Type objectType, object existingValue, JsonSerializer serializer)
            {//Convert to specific child class
                JObject jo = JObject.Load(reader);
                if (jo["type"].Value<string>() == "rgbcolor")
                    return jo.ToObject<ptvr.stimuli.RGBColor>(serializer);

                if (jo["type"].Value<string>() == "rgb255color")
                    return jo.ToObject<ptvr.stimuli.RGB255Color>(serializer);

                if (jo["type"].Value<string>() == "hexcolor")
                    return jo.ToObject<ptvr.stimuli.HexColor>(serializer);

                return null;
            }

            public override bool CanWrite
            {
                get { return false; }
            }

            public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
            {
                throw new System.NotImplementedException();
            }
        }
    }
}
