﻿//Helps converting between various display types as in ptvr.data.input
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace ptvr
{
    namespace Converters
    { 
        public class DisplayConverter : JsonConverter
        {
            public override bool CanConvert(System.Type objectType)
            {
                return (objectType == typeof(ptvr.data.input.Display));
            }

            public override object ReadJson(JsonReader reader, System.Type objectType, object existingValue, JsonSerializer serializer)
            {//Convert to specific child class
                JObject jo = JObject.Load(reader);
                if (jo["type"].Value<string>() == "user_option")
                    return jo.ToObject<ptvr.data.input.UserOptionDisplay> (serializer);

                if (jo["type"].Value<string>() == "timed")
                    return jo.ToObject<ptvr.data.input.TimedDisplay>(serializer);

                if (jo["type"].Value<string>() == "user_option_with_timeout")
                    return jo.ToObject<ptvr.data.input.UserOptionWithTimeoutDisplay>(serializer);

                if (jo["type"].Value<string>() == "user_option_with_sound_feedback")
                    return jo.ToObject<ptvr.data.input.UserOptionWithSoundFeedbackDisplay>(serializer);

                return null;
            }

            public override bool CanWrite
            {
                get { return false; }
            }

            public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
            {
                throw new System.NotImplementedException();
            }
        }
    }
}
