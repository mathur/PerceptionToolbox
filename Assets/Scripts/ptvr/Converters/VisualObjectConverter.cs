﻿//Helps converting between various color formats
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using UnityEngine;

namespace ptvr
{
    namespace Converters
    {
        public class VisualObjectTypeConverter : JsonConverter
        {
            public override bool CanConvert(System.Type objectType)
            {
                return (objectType == typeof(ptvr.stimuli.world.VisualObject));
            }

            public override object ReadJson(JsonReader reader, System.Type objectType, object existingValue, JsonSerializer serializer)
            {//Convert to specific child class
                JObject jo = JObject.Load(reader);

                if (jo["type"].Value<string>() == "cube")
                    return jo.ToObject<ptvr.stimuli.world.Cube>(serializer);

                if (jo["type"].Value<string>() == "sphere")
                    return jo.ToObject<ptvr.stimuli.world.Sphere>(serializer);

                if (jo["type"].Value<string>() == "quad")
                    return jo.ToObject<ptvr.stimuli.world.Quad>(serializer);

                if (jo["type"].Value<string>() == "cylinder")
                    return jo.ToObject<ptvr.stimuli.world.Cylinder>(serializer);

                if (jo["type"].Value<string>() == "text_box")
                    return jo.ToObject<ptvr.stimuli.world.TextBox>(serializer);
           
                return null;
            }

            public override bool CanWrite
            {
                get { return false; }
            }

            public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
            {
                throw new System.NotImplementedException();
            }
        }
    }
}
