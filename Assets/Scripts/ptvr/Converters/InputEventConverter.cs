﻿//Helps converting between various input event types in ptvr.data.input
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace ptvr
{
    namespace Converters
    {
        public class InputEventConverter : JsonConverter
        {
            public override bool CanConvert(System.Type objectType)
            {
                return (objectType == typeof(ptvr.data.input.InputEvent));
            }

            public override object ReadJson(JsonReader reader, System.Type objectType, object existingValue, JsonSerializer serializer)
            {//Convert to specific child class
                JObject jo = JObject.Load(reader);
                if (jo["type"].Value<string>() == "keyboard")
                    return jo.ToObject<ptvr.data.input.KeyboardEvent>(serializer);

                if (jo["type"].Value<string>() == "timed_keyboard")
                    return jo.ToObject<ptvr.data.input.TimedKeyboardEvent>(serializer);

                if (jo["type"].Value<string>() == "htc_vive")
                    return jo.ToObject<ptvr.data.input.HTCViveEvent>(serializer);

                if (jo["type"].Value<string>() == "timed_htc_vive")
                    return jo.ToObject<ptvr.data.input.TimedHTCViveEvent>(serializer);

                return null;
            }

            public override bool CanWrite
            {
                get { return false; }
            }

            public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
            {
                throw new System.NotImplementedException();
            }
        }
    }
}
