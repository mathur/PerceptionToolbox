﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ptvr
{
    namespace data
    {
        namespace input
        {
            public abstract class InputEvent
            {
                public string type; //The type of input event
            }

            public class KeyboardEvent : InputEvent
            {
                private string[] allowableKeys = {
                 "backspace",
                 "delete",
                 "tab",
                 "clear",
                 "return",
                 "pause",
                 "escape",
                 "space",
                 "up",
                 "down",
                 "right",
                 "left",
                 "insert",
                 "home",
                 "end",
                 "page up",
                 "page down",
                 "f1",
                 "f2",
                 "f3",
                 "f4",
                 "f5",
                 "f6",
                 "f7",
                 "f8",
                 "f9",
                 "f10",
                 "f11",
                 "f12",
                 "f13",
                 "f14",
                 "f15",
                 "0",
                 "1",
                 "2",
                 "3",
                 "4",
                 "5",
                 "6",
                 "7",
                 "8",
                 "9",
                 "!",
                 "\"",
                 "#",
                 "$",
                 "&",
                 "'",
                 "(",
                 ")",
                 "*",
                 "+",
                 ",",
                 "-",
                 ".",
                 "/",
                 ":",
                 ";",
                 "<",
                 "=",
                 ">",
                 "?",
                 "@",
                 "[",
                 "\\",
                 "]",
                 "^",
                 "_",
                 "`",
                 "a",
                 "b",
                 "c",
                 "d",
                 "e",
                 "f",
                 "g",
                 "h",
                 "i",
                 "j",
                 "k",
                 "l",
                 "m",
                 "n",
                 "o",
                 "p",
                 "q",
                 "r",
                 "s",
                 "t",
                 "u",
                 "v",
                 "w",
                 "x",
                 "y",
                 "z",
                 "numlock",
                 "caps lock",
                 "scroll lock",
                 "right shift",
                 "left shift",
                 "right ctrl",
                 "left ctrl",
                 "right alt",
                 "left alt"
                };
            }
            public class TimedKeyboardEvent : KeyboardEvent //TODO: Timed events do not make sense if you have a timed display
            {

            }

            public class HTCViveEvent : InputEvent
            {
                private string[] allowableKeys =
                {
                    "right_trigger",
                    "right_grip",
                    "right_touch_one",
                    "right_touch_two",
                    "left_trigger",
                    "left_grip",
                    "left_touch_one",
                    "left_touch_two"
                };

                public List<string> valid_responses; //This is obtained from the Python-end, defines valid responses, given a display
            }
            public class TimedHTCViveEvent : HTCViveEvent //TODO: Timed events do not make sense if you have a timed display
            {

            }

            //Associated with each scene so as to determine when the scene ends. May also indicate what needs to be stored
            public class Display
            {
                public string type; //Helps resolution to sub-classes
            }

            public class UserOptionDisplay : Display
            {
                public InputEvent events;
            }

            public class TimedDisplay : Display
            {
                public float ms; //The time for which the display is shown

                public float timeInSeconds()
                {
                    return ms / 1000f;
                }
            }

            public class UserOptionWithTimeoutDisplay : UserOptionDisplay
            {
                public float ms;

                public float timeInSeconds()
                {
                    return ms / 1000f;
                }
            }

            public class UserOptionWithSoundFeedbackDisplay : UserOptionDisplay
            {
                //Not yet implemented
            }
        }
    }
}