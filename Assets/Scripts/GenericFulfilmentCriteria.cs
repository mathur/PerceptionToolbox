﻿// Execute fulfilment criteria that is generic across different kinds of scenes ---> starts timer, sets up listener to events etc.

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenericFulfilmentCriteria : MonoBehaviour
{
    private bool[] ViveControllerButtonsCurrentlyActive = { false, false, false, false, false, false, false, false };

    public void SetFulfilmentVariables(ptvr.data.input.Display d) // What is the display type?
    {
        if (d == null)
            Debug.LogError("Cannot be null");

        switch (d.type)
        {
            case "user_option":
                //Terminate when a user option is given
                ptvr.data.input.UserOptionDisplay uod = (ptvr.data.input.UserOptionDisplay)d;
                switch(uod.events.type)
                {
                    case "htc_vive":
                        ptvr.data.input.HTCViveEvent hve = (ptvr.data.input.HTCViveEvent) uod.events;
                        if (hve.valid_responses.Count > 0)
                        {
                            foreach (string val_resp in hve.valid_responses)
                            {
                                //enable message receivers for respective controller buttons
                                switch(val_resp)
                                {
                                    case "right_trigger":
                                        ViveControllerButtonsCurrentlyActive[0] = true;
                                        break;
                                    case "right_grip":
                                        ViveControllerButtonsCurrentlyActive[1] = true;
                                        break;
                                    case "right_touch_one":
                                        ViveControllerButtonsCurrentlyActive[2] = true;
                                        break;
                                    case "right_touch_two":
                                        ViveControllerButtonsCurrentlyActive[3] = true;
                                        break;
                                    case "left_trigger":
                                        ViveControllerButtonsCurrentlyActive[4] = true;
                                        break;
                                    case "left_grip":
                                        ViveControllerButtonsCurrentlyActive[5] = true;
                                        break;
                                    case "left_touch_one":
                                        ViveControllerButtonsCurrentlyActive[6] = true;
                                        break;
                                    case "left_touch_two":
                                        ViveControllerButtonsCurrentlyActive[7] = true;
                                        break;
                                    default:
                                        Debug.LogError("Unknown response expected");
                                        break;
                                }
                            }
                        }
                        else
                            Debug.LogError("No valid responses specified for an event that is supposed to end on user input");
                        break;
                    default:
                        Debug.LogError("Invalid/Not yet implemented Input Event");
                        break;
                }
                break;

            case "timed":
                //Terminate after a given time
                ptvr.data.input.TimedDisplay td = (ptvr.data.input.TimedDisplay)d;
                Debug.Log("The display will end after " + td.timeInSeconds().ToString() + "seconds.");
                SendMessage("SetTimeForTrial", td.timeInSeconds());
                SendMessage("StartTimer");
                break;
            default:
                Debug.LogError("Invalid/Not yet implemented Display");
                break;
        }
    }

    public void UnSetFulfilmentVariables()
    {
        //Set every receiver switch to false
        for(int i =0; i< ViveControllerButtonsCurrentlyActive.Length; i++)
            ViveControllerButtonsCurrentlyActive[i] = false;
    }

    //Message receivers
    //Right controller
    public void RightGrip()
    {
        if (ViveControllerButtonsCurrentlyActive[0])
        {
            Debug.Log("Do something");
            SendMessage("Fulfilled", "right_grip");

        }
    }
    public void RightTrigger()
    {
        if (ViveControllerButtonsCurrentlyActive[1])
        {
            Debug.Log("Do something");
            SendMessage("Fulfilled", "right_trigger");
        }
    }
    public void RightTouchOne()
    {
        if (ViveControllerButtonsCurrentlyActive[2])
        {
            Debug.Log("Do something");
            SendMessage("Fulfilled", "right_touch_one");
        }
    }
    public void RightTouchTwo()
    {
        if (ViveControllerButtonsCurrentlyActive[3])
        {
            Debug.Log("Do something");
            SendMessage("Fulfilled", "right_touch_two");

        }
    }
    //Left controller
    public void LeftGrip()
    {
        if (ViveControllerButtonsCurrentlyActive[4])
        {
            Debug.Log("Do something");
            SendMessage("Fulfilled", "left_grip");

        }

    }
    public void LeftTrigger()
    {
        if (ViveControllerButtonsCurrentlyActive[5])
        {
            Debug.Log("Do something");
            SendMessage("Fulfilled", "left_trigger");

        }
    }
    public void LeftTouchOne()
    {
        if (ViveControllerButtonsCurrentlyActive[6])
        {
            Debug.Log("Do something");
            SendMessage("Fulfilled", "left_touch_one");
            
        }
    }
    public void LeftTouchTwo()
    {
        if (ViveControllerButtonsCurrentlyActive[7])
        {
            Debug.Log("Do something");
            SendMessage("Fulfilled", "left_touch_two");

        }
    }
}
