﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllerEventListener : MonoBehaviour {

    public bool isRight;

    public Transform SceneTemplatePlaceholder;
    public Transform FixationTemplate;
    public Transform TrialParent;

    private string prefixBasedOnController; //prefix based on left/right

    // Use this for initialization
    void Start ()
    {
        GetComponent<VRTK.VRTK_ControllerEvents>().GripPressed += new VRTK.ControllerInteractionEventHandler(DoGripPressed);
        GetComponent<VRTK.VRTK_ControllerEvents>().TriggerClicked += new VRTK.ControllerInteractionEventHandler(DoTriggerClicked);
        //The events for touch buittons are handled differently

        prefixBasedOnController = isRight ? "Right" : "Left"; //Get proper prefix
    }

    void DoGripPressed(object sender, VRTK.ControllerInteractionEventArgs e)
    {
        /*
        if (FixationTemplate.gameObject.activeSelf)
            FixationTemplate.GetComponent<FixationFulfilmentCriteria>().GripPressed();
            */
        SceneTemplatePlaceholder.BroadcastMessage(prefixBasedOnController + "Grip");
    }

    void DoTriggerClicked(object sender, VRTK.ControllerInteractionEventArgs e)
    {
        //TrialParent.GetComponent<TrialFulfilmentCriteria>().TriggerClicked();
        SceneTemplatePlaceholder.BroadcastMessage(prefixBasedOnController + "Trigger");
    }
}
