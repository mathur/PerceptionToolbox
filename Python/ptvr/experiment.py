"""
Interface for an experiment, which holds all information and structures related to the experiment being designed.
"""
import datetime
import json
import logging

class InvalidExperimentFile(Exception):
    pass

# This class is ultimately written to file
class Experiment:
    def __init__(self, name, output_file="experiment.txt"):
        self.name = name
        self.created = datetime.datetime.now()
        self.output_file = output_file
        self.scenes = [ ]

    def add_scene(self, individual_scene):  # Add a particular Scene object
        self.scenes.append(individual_scene)

    def write_metadata(self, fp):
        fp.write('"name" : "%s", "created" : "%s", \n' % (self.name, self.created))
        fp.write('"output_file" : "%s",\n' % self.output_file)
 

    def write_scenes(self, fp):
        fp.write('"scenes" : [ \n')
        if self.scenes:
            self.scenes[0].write(fp)
            for scene in self.scenes[1:]:
                fp.write(", \n")
                fp.write("    ")
                scene.write(fp)
        fp.write("]\n")

    def write(self):
        fp = open(self.output_file, 'w')
        fp.write("{")
        self.write_metadata(fp)
        self.write_scenes(fp)
        fp.write("}")
        fp.close()

def experiment_from_file(filename):
    e = Experiment("")
    try:
        with open(filename, "r") as fp:
            json_data = json.load(fp)
            logging.info("Read experiment data ")
            # now fill in the fields for e
            name = json_data.get('name', None) 
            if name is not None:
                e.name = name
            else:
                raise InvalidExperimentFile("Did not find field name in experiment file")
            created = json_data.get('created', None) 
            if created is not None:
                e.created = datetime.datetime.strptime(created, "%Y-%m-%d %H:%M:%S.%f")
            else:
                raise InvalidExperimentFile("Did not find field created in experiment")
            ofile = json_data.get('output_file', None) 
            if ofile is not None:
                e.output_file = ofile
            else:
                raise InvalidExperimentFile("Did not find field output_file in experiment")
            scenes = json_data.get('scenes', None)
            if scenes is not None:
                pass
            else:
                raise InvalidExperimentFile("Did not find scenes in experiment")
        return e
    except Exception as e:
        logging.error("Error opening file %s" % filename)
        logging.error(e)
        return None
