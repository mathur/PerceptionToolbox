"""
Interfaces for trial iteration
"""

import datetime

import data.store as datastore

class Trial(object):
    def __init__(self, experiment, participant, ds=datastore.CSVStore('data.csv'), run=1, online=False, server=None, log=True):
        self.experiment = experiment
        self.participant = participant
        self.results = ds
        self.run = run
        self.created = datetime.datetime.now()
        self.log = log
        self.online = online
        if online:
            if server is None:
                Exception("Trial: online mode requires a server")
            self.server = server
            self.check_server()

    def check_server(self):
        # perform sanity checks to make sure the server is up and running
        pass

    def run_trial(self):
        assert False, "run_trial should be implemented by the derived classes"

    def __getstate__(self):
        return { 'experiment' : self.experiment.name,
                 'participant' : self.participant,
                 'run'         : self.run,
                 'created'     : datetime.datetime.strftime(self.created, "%Y-%m-%d-%H:%M:%S"), 
                 'log'         : self.log,
                 'results'     : self.results.__getstate__(),
               }

class Iterator(Trial):
    """Simply display the sequence of scenes in the experiment in the order provided"""
    def __init__(self, experiment, participant, ds, online=False, server=None, run=1, log=True):
        super(Iterator, self).__init__(experiment, participant, ds, online=online, server=server, run=run, log=log)

    def run_trial(self):
        if self.online:
            pass
        else:
            # dump the information into a file that will be read by the server
            pass

    def __getstate__(self):
        s = super(Iterator, self).__getstate__()
        s['mode'] = 'iterator'
        return s

class Random(Trial):    
    """Display the sequence of scenes in the experiment in a random order"""
    def __init__(self, experiment, participant, ds, online=False, server=None, run=1, log=True):
        super(Random, self).__init__(experiment, participant, ds, online=online, server=server, run=run, log=log)

    def run_trial(self):
        if self.online:
            pass
        else:
            # dump the information into a file that will be read by the server
            pass
    def __getstate__(self):
        s = super(Iterator, self).__getstate__()
        s['mode'] = 'random'
        return s


class Staircase(Trial):
    """Not yet implemented: Dynamic connection required"""
    def __init__(self, experiment, participant, ds, online=False, server=None, run=1, others=None, log=True):
        super(Staircase, self).__init__(experiment, participant, ds, online=online, server=server, run=run, log=log)

    def run_trial(self):
        if self.online:
            pass
        else:
            # dump the information into a file that will be read by the server
            assert False, 'Staircase should be run in online mode'

    def __getstate__(self):
        s = super(Iterator, self).__getstate__()
        s['mode'] = 'staircase'
        return s


