"""
Interfaces for result storage-- currently not used and not implemented
"""

class DataStore(object):
    def open(self, mode="w"):
        assert False, "Derive in subclass"

    def close(self, mode="w"):
        assert False, "Derive in subclass"

    def write(self, mode="w"):
        assert False, "Derive in subclass"

    def __getstate__(self):
        assert False, "Derive in subclass"
    def __setstate__(self):
        assert False, "Derive in subclass"

class CSVStore(DataStore):
    def __init__(self, output_file):
        self.filename = output_file
        self.fp = None

    def open(self, mode='w'):
        if self.fp is None:
            self.fp = open(self.filename, mode)
            # XXX: catch error
            return self
        else:
            assert False, "File already open"
 
    def write(self, data):
        try:
            self.fp.write(data)
            self.fp.write('\n')
        except:
            assert False, "File write did not succeed"

    def close(self):
        self.fp.close()

    def __getstate__(self):
        return { "filename" : self.filename, "store" : "csv" }
    def __setstate__(self):
        assert False, "Derive in subclass"

class JsonStore(DataStore):
    pass

class MongoStore(DataStore):
    pass

class EncryptedCSVStore(DataStore):
    pass
