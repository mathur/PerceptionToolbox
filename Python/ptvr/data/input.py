"""
Interfaces to Input events and creating various kinds of displays, for example, a timeout display or a user option display.
"""

class InputEvent(object):
    pass


class KeyboardEvent(InputEvent):
    def __init__(self, valid_responses=set({'y', 'n'})):
        self.valid_responses = valid_responses

    def get_event(self):
        # get a keyboard event
        # and check it is a valid response
        return 'y'

class TimedKeyboardEvent(KeyboardEvent):
    pass


class HTCViveEvent(InputEvent):
    """
    The following buttons can be used-
        "right_trigger",
        "right_grip",
        "right_touch_one",
        "right_touch_two",
        "left_trigger",
        "left_grip",
        "left_touch_one",
        "left_touch_two"
    """
    def __init__(self, valid_responses=['right_touch_one', 'right_touch_two']):
        self.valid_responses = valid_responses

    def to_json(self):
        return dict(type="htc_vive", valid_responses=self.valid_responses)


class TimedHTCViveEvent(HTCViveEvent):
    pass


class Display(object):
    """Associate an event with each scene which determines when the scene ends. 
       A scene can end after a pre-determined time (e.g., show a stimulus for
       500 ms), or after a user input (e.g., a valid key press or a selection). 
       All such options are derived from this class. There can be more complex options, such as
       user input within a certain time, or a reminder each k ms, etc."""

    def show(self):
       # the stimulus is shown
       pass

    def get(self):
       # get the outcome
       pass


class UserOptionDisplay(Display):
    # the scene is shown until an user-level event is triggered
    def __init__(self, events=HTCViveEvent()):
        self.events = events

    def to_json(self):
        return dict(type="user_option", events=self.events)


class TimedDisplay(Display):
    def __init__(self, ms=1000):
        self.ms = ms

    def to_json(self):
        return dict(type="timed", ms=self.ms)


class UserOptionWithTimeoutDisplay(UserOptionDisplay):
    def __init__(self, ms=1000):
        super.__init__()
        self.ms = ms


class UserOptionWithSoundFeedbackDisplay(UserOptionDisplay):
    pass
