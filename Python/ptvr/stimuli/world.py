"""
Interfaces for creating scenes and objects within these scenes
"""

import numpy as np
import json

import ptvr.stimuli.color as color
import ptvr.data.input as input
from ptvr.stimuli.texture import Texture

PI = np.pi
TWOPI = 2 * PI


# Some predefined textures
diffuse_texture = Texture()


# Coordinate systems
class Frame(object):
    pass

class Cartesian(Frame):
    """
    A Cartesian Frame, defined by x,y and z coordinates centred at the centre of the tracked room.
    Positive x is in the forward direction.
    Positive y is in the upward direction.
    Positive z is in the left.
    """
    def __init__(self, pos=(0, 0, 0)):
        self.x = pos[0]
        self.y = pos[1]
        self.z = pos[2]

    def to_string(self):
        return str((self.x,self.y, self.z))


class Spherical(Frame):
    """
    A Spherical Frame, defined by radius and polar and elevation angle, centred at the centre of the tracked room.
    """
    def __init__(self, r=0.0, polar=0, elevation=0, units='degrees'):
        assert (r>= 0), "Spherical coordinate with negative radius"
        self.r = r
        if units == 'degrees':
            #assert (0 <= polar and polar <= 360), "Spherical coordinate with out-of-range polar angle"
            #assert (0 <= elevation and elevation <= 360), "Spherical coordinate with out-of-range elevation angle"
            self.polar = polar
            self.elevation = elevation
            self.units = units
            return
        if units == 'radians':
            #assert (0 <= polar and polar <= TWOPI), "Spherical coordinate with out of range polar angle"
            #assert (0 <= elevation and elevation <= TWOPI), "Spherical coordinate with out of range elevation angle"
            self.polar = polar
            self.elevation = elevation
            self.units = units
            return
        assert False, ("Unknown unit %s for angles" % units)

    def degree2radian(self):  # Helper function to convert internal variables from degree to radian
        assert self.units == 'degrees'
        self.polar = self.polar * (np.pi / 180)
        self.elevation = self.elevation * (np.pi / 180)
        self.units = 'radians'

    def to_string(self):
        return str((self.r,self.polar, self.elevation))

# def cart2spherical(c):
#     xy = c.x * c.x + c.y * c.y
#     r = np.sqrt(xy + c.z * c.z)
#     theta = np.arctan2(np.sqrt(xy), z) # for elevation angle defined from Z-axis down
#     phi = np.arctan2(c.y, c.x)
#     return Spherical(r, theta, phi, units='radians')


def spherical2cartesian(s):  # Helper function to convert spherical coordinates to cartesian
    assert s.units in ['degrees', 'radians']
    if s.units == 'radians':
        a = s.r * np.cos(s.elevation)
        x = a * np.cos(s.polar)
        y = s.r * np.sin(s.elevation)
        z = a * np.sin(s.polar)
        pos = np.array([x, y, z])
        return pos
    else:
        s.degree2radian()  # convert to radian
        return (spherical2cartesian(s))


class VisualObject(object):
    """
    The base class for a 3D visual object
    """
    def __init__(self, position=np.array([0.0, 0.0, 0.0]),
                       rotation=np.array([0.0, 0.0, 0.0]),
                       scale=np.array([1.0, 1.0, 1.0])):
        self.position = position
        self.rotation = rotation
        self.scale = scale

    def to_json(self):
        print("In VisualObject: ", self.__dict__)
        return json.dumps(self.__dict__)


class VisualObjectDecorator(VisualObject):
    def __init__(self, stimulus):
        self._stimulus = stimulus


class TextBox(VisualObject):
    """
    A textbox that can be placed in a visual scene.
    """
    def __init__(self, text='Your Text Here',
                       bold=False,
                       italic=False,
                       font_size=64,
                       font_color=color.RGBColor()):
        # self.name = name
        self.text = text
        # self.font_name = font_name
        self.bold = bold
        self.italic = italic
        self.font_size = font_size
        self.font_color = font_color
        # self.align_horizontal = align_horz  # fixed from self.align_horizontal = align_horizontal
        # self.align_vertical = align_vert  # fixed from self.align_vertical = align_vertical

    def to_json(self):
        return dict(type="text_box", text=self.text, bold=self.bold,
                    italic=self.italic, font_size=self.font_size, font_color=self.font_color)


class Cube(VisualObject):
    """
    A cube that can be placed in a visual scene.
    """
    def __init__(self, side=1, color=color.RGBColor(r=0.0,g=0.0,b=0.0,a=0.0), texture=diffuse_texture):
        self.side = side
        self.color = color
        self.texture = texture

    def __str__(self):
        return "Cube(side=%s,color=%s)" % (self.side, self.color)

    def to_json(self):
        return dict(type="cube", side=self.side, color=self.color, texture=self.texture)


class Sphere(VisualObject):
    """
    A sphere that can be placed in a visual scene.
    """
    def __init__(self, radius=1, color=color.RGBColor(r=0.0,g=0.0,b=0.0,a=0.0), texture=diffuse_texture):
        self.radius = radius  # fixed from self.radius = side
        self.color = color
        self.texture = texture

    def to_json(self):
        return dict(type="sphere", radius=self.radius, color=self.color, texture=self.texture)


class Cylinder(VisualObject):
    """
    A cylinder that can be placed in a visual scene.
    """
    def __init__(self, radius=1, height=1,color=color.RGBColor(r=0.0,g=0.0,b=0.0,a=0.0), texture=diffuse_texture):
        self.radius = radius  # fixed from self.radius = side
        self.color = color
        self.height = height
        self.texture = texture

    def to_json(self):
        return dict(type="cylinder", radius=self.radius,  height=self.height,
                    color=self.color, texture=self.texture)


class Quad(VisualObject):
    """
    A quad that can be placed in a visual scene.
    A quad has normals in only one direction and appears invisible from the opposite face
    """
    def __init__(self, side=1, color=color.RGBColor(r=0.0,g=0.0,b=0.0,a=0.0), texture=diffuse_texture):
        self.side = side
        self.color = color
        self.texture = texture

    def to_json(self):
        return dict(type="quad", side=self.side, color=self.color, texture=self.texture)


class STLObject(VisualObject):
    pass


class ComplexEncoder(json.JSONEncoder):
    """
    An internal class that helps in encoding of objects that are and aren't directly serializable
    """
    def default(self, obj):
        if isinstance(obj, np.ndarray):
            return obj.tolist()

        if hasattr(obj,'to_json'):
            return obj.to_json()
        else:
            return json.JSONEncoder.default(self, obj)


class Scene(object):
    """
    The base class for a scene. An experiment consists of several consecutive scenes.
    """
    def to_json(self):
        assert False, "to_json should be derived in every subclass"

    def write(self, fp):
        fp.write(json.dumps(self.to_json(), sort_keys=True, indent=4, cls=ComplexEncoder))


class FixationScene(Scene):
    """
    Interface for generating a Fixation scene. This is  a replacement for the ubiquitous fixation screens in
    traditional psychophysics experiments.
    """
    def __init__(self, id=-1, text="Place one foot on each box on the floor.\n"
                                   "Look directly at the fixation cross and press the side buttons to start.",
                 display=input.UserOptionDisplay(events=input.HTCViveEvent(valid_responses=['right_grip', 'left_grip']))):
        self.id = id
        self.text = text
        self.input = display

    def to_json(self):
        return dict(type="FIXATION_SCENE", id=self.id, text=self.text, display=self.input)


class VisualScene(Scene):
    """
    Interface for generating a Visual scene.
    A visual scene can be populated with visual stimuli so as to create a trial.
    """
    def __init__(self, id=-1, background_color=color.RGBColor(1,1,1,1) , display=input.TimedDisplay()):
        self.id = id
        self.objects = [ ]
        self.input = display
        self.background_color = background_color

    def place(self, o, position=np.array([0, 0, 0]), orientation=np.array([0, 0, 0])):
        self.objects.append((o, position, orientation))

    def _pickle_object(self, o, pos, rot):
        #return json.dumps({ "object" : o.to_json(),
        #                    "position" : pos.tolist(),
        #                   "orientation" : rot.tolist() })
        return dict(obj=o, position=pos, orientation=rot)

    def to_json(self):
        print(self.objects)
        objects = [self._pickle_object(o, pos, orien) for (o, pos, orien) in self.objects]
        print(objects)
        return dict(type="VISUAL_SCENE", id=self.id, background_color=self.background_color,
                    objects=objects, display=self.input)


class ResponseScene(Scene):
    """
    An interface for creating a scene where user response is requested.
    """
    def __init__(self, id=-1, text="What is your response?", display=input.UserOptionDisplay(events=input.HTCViveEvent(
        valid_responses=['right_touch_one', 'right_touch_two', 'left_touch_one', 'left_touch_two']))):
        self.id = id
        self.text = text
        self.input = display

    def to_json(self):
        return dict(type="RESPONSE_SCENE", id=self.id, text=self.text, display=self.input)


