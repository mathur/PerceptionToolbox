"""
A simple example using the library
"""

import numpy as np
from ptvr import experiment
import ptvr.stimuli.world 
import ptvr.data.input as input


def main():
    e = experiment.Experiment(name="My first experiment in VR", output_file="test.txt")

    fs = ptvr.stimuli.world.FixationScene(text="Get back to the centre and look at the cross.")
    e.add_scene(fs)

    ts = ptvr.stimuli.world.VisualScene(id=1, display=input.TimedDisplay(ms=5000))
    pos = np.array([0.0, 0.0, 0.0])
    for i in range(1, 8):
        ts.place(ptvr.stimuli.world.Cube(), pos)
        pos = pos + np.array([1, 1, 1])
    e.add_scene(ts)

    e.add_scene(ptvr.stimuli.world.ResponseScene(text="Did you see 7 cubes?\n[1]Yes\n[2]No"))

    # Finally, write the experiment (inclusive of added scenes to the output_file
    e.write()


if __name__ == "__main__":
    main()
