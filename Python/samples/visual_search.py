""""
An experiment to study visual search in various 3D regions
"""

import numpy as np
import random
import ptvr.experiment
import ptvr.stimuli.world
import ptvr.data.input as input
import ptvr.stimuli.color as color


def main():
    print("Creating an experiment to study visual search in various 3D regions...")

    # Set-up the experiment file
    exp = ptvr.experiment.Experiment(name="VisualSearchExperiment", output_file="visual_search.txt")

    """
    # Add a fixation screen with info on the experiment
    fs = ptvr.stimuli.world.FixationScene(text="In this experiment, you shall see a red cube, which is the target "
                                               "and various other objects. You need to find the target."
                                               "\nPress side buttons to continue.")
    exp.add_scene(fs)
    """

    num = 0  # A variable to store id numbers
    # Visual scene that holds all the visual objects
    t1 = ptvr.stimuli.world.VisualScene(id=num, display=input.UserOptionDisplay(events=input.HTCViveEvent
                                        (valid_responses=["right_trigger", "left_trigger"])))

    # Initialize elevation and polar angles
    polar_array = np.array([0, 45, 90, 135, 180, 225, 270, 315, 360])
    elevation_array = np.array([-90, -45, 0, 45, 90])

    # Initialize minimum and maximum
    radius = np.array([2, 7])

    height = np.array([0.0, 1.5, 0.0])  # Height at which stimulus is placed

    square = ptvr.stimuli.world.Cube(side=0.2, color=color.RGBColor(r=0, g=1, b=0, a=1))

    for elevation_id in range(len(elevation_array)-1):
        # For each element in the elevation angle array
        for polar_id in range(len(polar_array)-1):
            # For each element in the polar angle array
            # Get an appropriate spherical coordinate randomly between certain ranges
            spherical_coord = ptvr.stimuli.world.Spherical(r=random.uniform(radius[0], radius[1]),
                                                           polar=random.randint(polar_array[polar_id], polar_array[polar_id+1]),
                                                           elevation=random.randint(elevation_array[elevation_id], elevation_array[elevation_id+1]))
            # Convert to cartesian
            cartesian = ptvr.stimuli.world.spherical2cartesian(spherical_coord)
            # Place square in the chosen location, with no rotation
            t1.place(square, cartesian + height, np.array([0.0, 0.0, 0.0]))

    # Create and place the target
    target = ptvr.stimuli.world.Cube(side=0.2, color=color.RGBColor(r=1, g=0, b=0, a=1))
    target_pos_spherical = ptvr.stimuli.world.Spherical(5.0, 60, 10)
    target_pos_cartesian = ptvr.stimuli.world.spherical2cartesian(target_pos_spherical)

    # Place the target in the Visual Scene
    t1.place(target, target_pos_cartesian + height, np.array([0.0, 0.0, 0.0]))

    # Add this scene to the experiment
    exp.add_scene(t1)

    # Increment identifier of scenes
    num += 1
    # Add a response scene to ask the participant if he/she saw the target
    exp.add_scene(ptvr.stimuli.world.ResponseScene(id=num, text="Did you see the red cube "
                                                                "\n[1]Yes\n[2]No"))

    # Write the experiment to file
    exp.write()
    print("The experiment has been written.")


if __name__ == "__main__":
        main()
