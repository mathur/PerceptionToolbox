PerceptionToolBox Python Scripts
=========================


To run the python scripts, please follow the below setups.

- Download python 3.6.3. Link[https://www.python.org/downloads/release/python-363/]
- Download Windows X86-64 executable installer for windows from the above link.
- Download MongoDB for data storage and data manupulation. Link[https://www.mongodb.com/download-center#previous]
- Download version 3.4.13 for windows.

### Contents of the repository:

- ````contrib```` directory contains all python scripts for data generation and manipulation
- ````AuxiliaryScripts```` directory contains main python scripts for data manipulation after mongodb connection
- ````ExperimentImages```` directory contains blank folder. Run script from the directory AuxiliaryScripts, named 'MongoDBtoGraphBySubjectID', all graphs would be generated and stored in this directory.
- ````WriteToMongoDB```` Sample python scripts to write example data in mongodb server.(Developer should change the data based on the requirements, as the sample data is hardcoded now)


### Building and running the python scripts:

Requirements:

- Python 3.6.3
- MongoDB 3.4.13

### Installation
# MongoDB Setup Procedures:
- Create a folder in C: drive naming as mongoDB.
- Install the mongodb exe in this pre-defined location like C:\mongoDB
- After installing mongodb the folder structure would be like "C:\mongoDB\.."
- Go to "C:\mongoDB" and add two folder naming "data" and "log".
- Under the "data" folder add another folder naming "db".
- Open commandpromt to start the mongo service.
- In commandpromt use following commands:
```
cd C:\mongoDB\bin
mongod --directoryperdb --dbpath C:\mongoDB\data\db --logpath C:\mongoDB\log\mongo.log --logappend --rest --install
net start MongoDB
mongo
```
- Mongoservice is started and below commands is for creating database and collection in mongodb.
```
1. db
2. use testDB[Your new DB name]
3.db.CreateCollection('ExcelData')[Your Collection Name]
4.show collection
5.run script excel to mongo(only 1 time)
6.db.ExcelData.find()[Check data exntry in collections]
7.db.ExcelData.remove({})[If you want to remove all data from collections]
8.If you delete the data run the script(ExceltoMongoDB) again but for one time only.Multiple time run this scripts will stack the same data into the collectio.So be careful:)
```
# Python Setup Procedures:
Install python in your system in mentioned location.(Example: C:\ProgramFiles\Python36)
Open commandpromt to install pre-requisite python libraries.
In commandpromt use following commands:
```
cd C:\Program Files\Python36 (Installation Location)
pip install numpy
pip install scipy
pip install pymongo (mongo driver to connect with python)
pip install radint
pip install configparser
pip install xlrd
pip install json
pip install pandas
pip install matplotlib
pip install plotly
```

Run the example python scripts:

```
ExcelToMongoDB script is responsible for fetching data from excel and dump into mongoDB database.
MongoDBtoGraphBySubjectID script is responsible for fetching data from mongodb and generate graphs.
MongoToGraph is sample script for generating graph based on mongodb data.
ReactionTimeInAsendingOrderForSubjectID script is responsible for generate excel file in asecending order based on
mongodb data.
WriteToMongoDB is responsible for write a dummy data into mongodb database.
```

Make sure that the mongodb connects to the localhost server.[mongodb://localhost:27017/] 

### Use configuration file for running the python scripts:

- configuration file location:
[contrib\AuxiliaryScripts\Configuration File]
- current configuration of config file, user may change based on requirements.
-[SectionMongoConnection] -> mongodb localhost server.
-[SectionMongoDataBase] -> mongodb database name.(can be changed if you wish to create and dump data to another db)
-[SectionExcelFileLocation] -> excelfile location(local).(can be changed if you wish)
-[SectionCollectionNameForExcelImport] -> mongodb collection name for dumping excel file data.(can be changed if you wish to create and dump data to another collection)
-[SectionCollectionNameForMongoImport] -> mongodb collection name for dumping sample  data.(can be changed if you wish to create and dump data to another collection)
-[SectionDirectoryForImport] -> file location to export graphs by script.(can be changed if you wish)

- Configure options in the config file:
```
[SectionMongoConnection]
Connection: mongodb://localhost:27017/

[SectionMongoDataBase]
DataBase = testDB

[SectionExcelFileLocation]
FileLocation = E:\Developed Perception-ToolBox-Code\PerceptionToolbox-master-code\PythonScripts\DataInterpretation\AuxiliaryScripts\Configuration File\merged.xlsx

[SectionCollectionNameForExcelImport]
ExcelDataImport = ExcelData

[SectionCollectionNameForMongoImport]
PythonToMongoDB = reviews

[SectionDirectoryForImport]
PlotImport = E:\\Developed Perception-ToolBox-Code\\PerceptionToolbox-master-code\\PythonScripts\\DataInterpretation\\ExperimentImages

```

- Make sure that exerything should be installed before you run the scripts:
    -Exception is not properly handled, so make sure you do the setup properly.



