"""This script connects to a local MongoDB instance and write 
the sample data to a mongo database collection .
Author: Anirban Ghosh
Version: 0.1
InputFile: 1. "Read Configuration File which must be located on a folder called AuxiliaryScripts then another folder Configuration File."
OutputFile: 1. "Write sample data into mongodb collection, and the collection name should be specified into confoguration file. User can change collection name as per requirement."
"""
#imported Library
#import for conecting mongo using python #pymongo
from pymongo import MongoClient
from random import randint
import os.path
import sys
#import configparser for reading the configuration file
import configparser
#Load config file to configparser object
Config = configparser.ConfigParser()
#For accessing the file in the parent folder of the current folder
script_dir = os.path.dirname(__file__)
rel_path = "AuxiliaryScripts\Configuration File\config.ini"
abs_file_path = os.path.join(script_dir, rel_path)
#This Fucntion is responsible for checking the file exist
#into the given location or not.
#It should throws exception if file does not exist.
#It also check that proper config file is exist or not like same name of file can exist on the located folder with
#diffrent extension. 
#This function take input of the desired configuration file path where the configration file should located.
def checkFileExist(file_path):
    try:
        if (os.path.exists(file_path)):
            print("Anirban")
            Config.read(file_path)
            
            return True
        else:
            print("File does not exist!!")
            return False
            sys.exit(1)
    except Exception as e:    
        print(e, ":File does not exist")
checkFileExist(abs_file_path)
#function to read from configuration file.
#This function take input of each section from configuration file.
#It returns the values and keys from configuration file.(Like each section has key which is in left side and values in right side.) 
def ConfigSectionMap(section):
    dictMongo = {}
    options = Config.options(section)
    for option in options:
        try:   
            dictMongo[option] = Config.get(section, option)
            if dictMongo[option] == -1:
                DebugPrint("skip: %s" % option)
        except:
            print("exception on %s!" % option)
            dictMongo[option] = None
    return dictMongo
#Global variable for connectionstring and database name.
#read from the configuration file for mongodb connection port. 
Connection_String = ConfigSectionMap("SectionMongoConnection")['connection']
#read from the configuration file for mongodb database name.
Database_Name = ConfigSectionMap("SectionMongoDataBase")['database']
#read from the configuration file for upload into mongodb collection. 
Python_Collection_Import = ConfigSectionMap("SectionCollectionNameForMongoImport")['pythontomongodb']
#Step 2: Create sample data
names = ['Kitchen','Animal','State', 'Tastey', 'Big','City','Fish', 'Pizza','Goat', 'Salty','Sandwich','Lazy', 'Fun']
company_type = ['LLC','Inc','Company','Corporation']
company_cuisine = ['Pizza', 'Bar Food', 'Fast Food', 'Italian', 'Mexican', 'American', 'Sushi Bar', 'Vegetarian']
#Step 1: Connect to MongoDB - Note: Change connection string as needed
#this fucntion takes connection string and database name as input.
#it stores the sample data into mongodb collection. data must be into dictionary format.
def CreateConnection(connection_string,db_name):
	client = MongoClient(connection_string)
	db = client[db_name]
	account=db[Python_Collection_Import]
	#random 500 lines of data stored into mongodb collection.
	for x in range(1, 501):
		business = {
        'name' : names[randint(0, (len(names)-1))] + ' ' + names[randint(0, (len(names)-1))]  + ' ' + company_type[randint(0, (len(company_type)-1))],
        'rating' : randint(1, 5),
        'cuisine' : company_cuisine[randint(0, (len(company_cuisine)-1))] 
		}
		#Step 3: Insert business object directly into MongoDB via insert_one
		result=account.insert_one(business)
		#Step 4: Print to the console the ObjectID of the new document
		print('Created {0} of 100 as {1}'.format(x,result.inserted_id))
	#Step 5: Tell us that you are done
	print('finished creating 100 business reviews')
	return;
#Call Function to create connection in MongoDB and insert data into the database
CreateConnection(Connection_String,Database_Name)

