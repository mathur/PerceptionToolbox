"""This script connects to a local MongoDB instance and write 
the desired excel sheet data to a particular mongo collection database.
Author: Anirban Ghosh
Version: 0.1
InputFile: 1. "Read Configuration File which must be located on a folder called Configuration File under the same folder from where the script is running."
		   2. "Read or Write Excel File which must be located on a folder called Configuration File under the same folder from where the script is running.(Example: here the excel file name is merged.xlsx)"
OutputFile: 1. "Write data of excel into mongodb collection, and the collection name should be specified into confoguration file. User can change collection name as per requirement."
"""
#imported Library
import xlrd
import sys
import os.path
import json
#import for conecting mongo using python #pymongo
import pymongo
import configparser
#Import client for networking or establish connection between python and mongodb
from pymongo import MongoClient
#Load config file to configparser object
Config = configparser.ConfigParser()
#For accessing the file in the parent folder of the current folder
script_dir = os.path.dirname(__file__)
#Here is the input file path fo configuration file.
rel_path = "Configuration File\config.ini"
abs_file_path = os.path.join(script_dir, rel_path)
#This Fucntion is responsible for checking the file exist
#into the given location or not.
#It should throws exception if file does not exist.
#It also check that proper config file is exist or not like same name of file can exist on the located folder with
#diffrent extension. 
#This function take input of the desired configuration file path where the configration file should located.
def checkFileExist(file_path):
    try:
        if (os.path.exists(file_path)):
            if (file_path.endswith('.ini')):
                Config.read(file_path)          
            return True
        else:
            print("File does not exist!!")
            return False
            sys.exit(1)
    except Exception as e:    
        print(e, ":File does not exist")
#Call Function to check file exist
#or not before the actual execution begin.
checkFileExist(abs_file_path)
#function to read from configuration file.
#This function take input of each section from configuration file.
#It returns the values and keys from configuration file.(Like each section has key which is in left side and values in right side.) 
def ConfigSectionMap(section):
    dictMongo = {}
    options = Config.options(section)
    for option in options:
        try:
            dictMongo[option] = Config.get(section, option)
            if dictMongo[option] == -1:
                DebugPrint("skip: %s" % option)
        except:
            print("exception on %s!" % option)
            dictMongo[option] = None
    return dictMongo
#Global variable for connectionstring and database name.
#read from the configuration file for mongodb connection port.  
Connection_String = ConfigSectionMap("SectionMongoConnection")['connection']
#read from the configuration file for mongodb database name.  
Database_Name = ConfigSectionMap("SectionMongoDataBase")['database']
#read from the configuration file for input excel file.  
Excel_File_Location = ConfigSectionMap("SectionExcelFileLocation")['filelocation']
#read from the configuration file for mongodb collection for data storage from excel.  
Excel_Collection_Import = ConfigSectionMap("SectionCollectionNameForExcelImport")['exceldataimport']
#This Function is responsible for creating connection in mongodb
#and dump the data into mondb database for further manipulation.
#This function takes input as mongodb connection string and mongodb data base name.
def CreateConnection(connection_string,db_name):
	#Load configuration file input into local variables.
	client = MongoClient(connection_string)
	db = client[db_name]
	account=db[Excel_Collection_Import]
	#Open Excel workbook. 
	data=xlrd.open_workbook(Excel_File_Location)
	#taking data from sheet 0, change it if required.
	table=data.sheets()[0]
	# read excel The first line of data is stored as a mongodb Field name
	rowstag=table.row_values(0)
	nrows=table.nrows
	returnData={}
	#Iterate row by row after having all data from excel.
	for i in range(1,nrows):
		# Field name and excel Data is stored as a dictionary. ， And converted to json format
		returnData[i]=json.dumps(dict(zip(rowstag,table.row_values(i))))
		# Decode and restore data
		returnData[i]=json.loads(returnData[i])
		#print returnData[i]
		account.insert(returnData[i])
#Call Function to create connection in MongoDB and insert data into the database
#Application or script start-up point.
try:
    if checkFileExist(Excel_File_Location):
        CreateConnection(Connection_String,Database_Name)
        print("Data Upload Complete")
        
except Exception as e:    
    print(e, ":File does not exist")
	

