"""This script is use to connect into a local MongoDB instance and
generates desired graphes using Plotly library of python using the data stored in mongoDB.
Author: Anirban Ghosh
Version: 0.1
InputFile: 1. "Read Configuration File which must be located on a folder called Configuration File under the same folder from where the script is running."
OutputFile: 1. "Generate graph and stored in a specified location, and the location name should be specified into confoguration file. User can change location name as per requirement."
"""
#imported library
import datetime
import plotly
#Import client for networking or establish connection between python and mongodb
from pymongo import MongoClient
import os.path
import sys
#Import library for generating graph.
from plotly.graph_objs import Layout, Scatter
import matplotlib.pyplot as plt
import matplotlib.ticker as plticker
import numpy as np
import matplotlib.ticker as ticker
#import configparser for reading the configuration file
import configparser
#Load config file to configparser object
Config = configparser.ConfigParser()
#function to read from configuration file.
#This function take input of each section from configuration file.
#It returns the values and keys from configuration file.(Like each section has key which is in left side and values in right side.) 
def ConfigSectionMap(section):
    dictMongo = {}
    options = Config.options(section)
    for option in options:
        try:
            dictMongo[option] = Config.get(section, option)
            if dictMongo[option] == -1:
                DebugPrint("skip: %s" % option)
        except:
            print("exception on %s!" % option)
            dictMongo[option] = None
    return dictMongo
#function main or startup location of the script to generate graphs.
def main():
    """Main Function
    """
    #Read Configuration file for configure mongoDB
    #File Location
    #For accessing the file in the parent folder of the current folder
    script_dir = os.path.dirname(__file__)
    rel_path = "Configuration File\config.ini"
    abs_file_path = os.path.join(script_dir, rel_path)
    try:
        if (os.path.exists(abs_file_path)):
            Config.read(abs_file_path)
        else:
            print("File does not exist!!")
            sys.exit(1)
    except Exception as e:    
        print(e, ":File does not exist")
	#blank arrays for x and y axis values.
    excel_type_conjunctive_numobj = []
    excel_type_feature_numobj = []
    excel_reaction_time_conjunctive = []
    excel_reaction_time_feature = []
    excel_subject_id = []
    excel_region_number = []
    
    #Global variable for connectionstring and database name
    #read from the configuration file for mongodb connection port. 
    Connection_String = ConfigSectionMap("SectionMongoConnection")['connection']
	#read from the configuration file for mongodb database name.
    Database_Name = ConfigSectionMap("SectionMongoDataBase")['database']
	#read from the configuration file for mongodb collection for data manupulation. 
    Excel_Collection_Import = ConfigSectionMap("SectionCollectionNameForExcelImport")['exceldataimport']
    #connect to MongoDB for data visualization
	#Load configuration file input into local variables.
    client = MongoClient(Connection_String)
    db = client[Database_Name]
    account=db[Excel_Collection_Import]

    try:
		#distinct subjectid and region number
        excel_subject_id = account.distinct("SubjectID")
        excel_region_number = account.distinct("RegionNum")
    except Exception as err:
        print(err)
        
    try:
        for subject_id in range(len(excel_subject_id)):
			#hard-coded file path to store images of the graph. change as per requirement based on the requirement.
            filepath = r'E:\Developed Perception-ToolBox-Code\PerceptionToolbox-master-code\PythonScripts\DataInterpretation\ExperimentImages\{}'.format(excel_subject_id[subject_id]) 
            if not os.path.exists(filepath):
                os.makedirs(filepath)
			# 1st for loop is for taking each region number.
            for region_number in range(len(excel_region_number)):
				#this is for conjunctive search.
                for rec in account.find({"TypeOfSearch":"Conjunctive", "SubjectID" : excel_subject_id[subject_id], "RegionNum" : excel_region_number[region_number]}):
                        excel_type_conjunctive_numobj.append(rec['NumOfObjects'])
                        excel_reaction_time_conjunctive.append(rec['ReactionTime'])
				# this is for feature search.
                for rec in account.find({"TypeOfSearch":"Feature", "SubjectID" : excel_subject_id[subject_id], "RegionNum" : excel_region_number[region_number]}):
                        excel_type_feature_numobj.append(rec['NumOfObjects'])
                        excel_reaction_time_feature.append(rec['ReactionTime'])
                plt.xticks(excel_type_conjunctive_numobj)
                ##Change y axis value as per requirement.
                plt.ylim(0,125)
                ax = plt.axes()
                ax.yaxis.set_major_locator(ticker.MultipleLocator(25))
                plt.xlabel("Number Of Objects")
                plt.ylabel("Reaction Time")
                plt.plot(excel_type_conjunctive_numobj, excel_reaction_time_conjunctive, 'r', label="Conjunction Search")
                plt.plot(excel_type_feature_numobj, excel_reaction_time_feature, 'b', label="Feature Search")
                plt.legend()
                #graph generated in desired location which is hard coded, make sure you can change before run the script.
                plt.savefig('E:\\Developed Perception-ToolBox-Code\\PerceptionToolbox-master-code\\PythonScripts\\DataInterpretation\\ExperimentImages\\{}\\{}_{}.png'.format(excel_subject_id[subject_id],excel_subject_id[subject_id],int(excel_region_number[region_number])))
                plt.clf()
				#clear all arrays after successful generation of graphs.
                excel_type_conjunctive_numobj.clear()
                excel_reaction_time_conjunctive.clear()
                excel_type_feature_numobj.clear()
                excel_reaction_time_feature.clear()
        
    except Exception as err:
        print(err)
    print("Image Upload Complete.")
#Application start-up point 
#main function call 
if __name__ == "__main__":
    main()
