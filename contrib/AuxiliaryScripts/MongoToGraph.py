"""This script is use to connect into a local MongoDB instance and
generates desired graphes using Plotly library of python using the data stored in mongoDB.
Author: Anirban Ghosh
Version: 0.1
InputFile: 1. "Read Configuration File which must be located on a folder called Configuration File under the same folder from where the script is running."
OutputFile: 1. "Generate graph and stored in a specified location, and the location name should be specified into confoguration file. User can change location name as per requirement."
"""
#imported Library
import datetime
import plotly
#Import client for networking or establish connection between python and mongodb
from pymongo import MongoClient
import os.path
import sys
#Import library for generating graph.
from plotly.graph_objs import Layout, Scatter
#import configparser for reading the configuration file
import configparser
#Load config file to configparser object
Config = configparser.ConfigParser()
#function to read from configuration file.
#This function take input of each section from configuration file.
#It returns the values and keys from configuration file.(Like each section has key which is in left side and values in right side.) 
def ConfigSectionMap(section):
    dictMongo = {}
    options = Config.options(section)
    for option in options:
        try:
            dictMongo[option] = Config.get(section, option)
            if dictMongo[option] == -1:
                DebugPrint("skip: %s" % option)
        except:
            print("exception on %s!" % option)
            dictMongo[option] = None
    return dictMongo
#function main or startup location of the script to generate graphs.
def main():
    """Main Function
    """
    #Read Configuration file for configure mongoDB
    #For accessing the file in the parent folder of the current folder
    script_dir = os.path.dirname(__file__)
    rel_path = "Configuration File\config.ini"
    abs_file_path = os.path.join(script_dir, rel_path)
    try:
        if (os.path.exists(abs_file_path)):
            Config.read(abs_file_path)
        else:
            print("File does not exist!!")
            sys.exit(1)
    except Exception as e:    
        print(e, ":File does not exist")
	#blank arrays for x and y axis values.
    excel_type_conjunctive_numobj = []
    excel_type_feature_numobj = []
    excel_reaction_time_conjunctive = []
    excel_reaction_time_feature = []
    
    #Global variable for connectionstring and database name.
	#read from the configuration file for mongodb connection port. 
    Connection_String = ConfigSectionMap("SectionMongoConnection")['connection']
	#read from the configuration file for mongodb database name.
    Database_Name = ConfigSectionMap("SectionMongoDataBase")['database']
	#read from the configuration file for mongodb collection for data manupulation. 
    Excel_Collection_Import = ConfigSectionMap("SectionCollectionNameForExcelImport")['exceldataimport']

    #connect to MongoDB for data visualization.
	#Load configuration file input into local variables.
    client = MongoClient(Connection_String)
    db = client[Database_Name]
    account=db[Excel_Collection_Import]
    
    try:
		#load values for Conjunctive search.
        for rec in account.find({"TypeOfSearch":"Conjunctive"}):
            excel_type_conjunctive_numobj.append(rec['NumOfObjects'])
            excel_reaction_time_conjunctive.append(rec['ReactionTime'])
    except Exception as err:
        print(err)
        
    try:
		#load values for Feature search.
        for rec in account.find({"TypeOfSearch":"Feature"}):
            excel_type_feature_numobj.append(rec['NumOfObjects'])
            excel_reaction_time_feature.append(rec['ReactionTime'])
    except Exception as err:
        print(err)
    #use scatter for converting the data into graph
	#for conjunctive
    excel_conjunctive_line = Scatter(
        y=excel_reaction_time_conjunctive,
        x=excel_type_conjunctive_numobj,
        name='Conjunctive Plot',
        mode='lines+markers')
    #for feature
    excel_feature_line = Scatter(
        y=excel_reaction_time_feature,
        x=excel_type_feature_numobj,
        name='Feature Plot',
        mode='lines+markers')

    #Plot the graph into html file
	#show graphs into local html file.
    plotly.offline.plot(
        {
            'data': [excel_conjunctive_line,excel_feature_line],
            'layout': Layout(title="Conjunctive Fetaure Graph")},
        filename=r"ConjunctiveFetaure.html", auto_open=True)
#Application start-up point 
#main function call 
if __name__ == "__main__":
    main()
