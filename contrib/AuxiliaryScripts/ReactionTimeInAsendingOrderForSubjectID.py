"""This script connects to a local MongoDB instance and
generates a Excel for SubjectID and ReactionTime in Ascending Order. 
Author: Anirban Ghosh
Version: 0.1
InputFile: 1. "Read Configuration File which must be located on a folder called Configuration File under the same folder from where the script is running."
OutputFile: 1. Write Excel File which must be located on a folder called Configuration File under the same folder from where the script is running.(Example: here the excel file name is ConjunctiveSearchSortedData.xlsx)
				and FeatureSearchSortedData.xlsx"
"""
#imported library
import xlrd
import datetime
#Import client for networking or establish connection between python and mongodb
from pymongo import MongoClient
import os.path
import sys
import numpy as np
import pandas as pd
from operator import itemgetter, attrgetter, methodcaller
#import configparser for reading the configuration file
import configparser
#Load config file to configparser object
Config = configparser.ConfigParser()
#function to read from configuration file.
#This function take input of each section from configuration file.
#It returns the values and keys from configuration file.(Like each section has key which is in left side and values in right side.) 
def ConfigSectionMap(section):
    dictMongo = {}
    options = Config.options(section)
    for option in options:
        try:
            dictMongo[option] = Config.get(section, option)
            if dictMongo[option] == -1:
                DebugPrint("skip: %s" % option)
        except:
            print("exception on %s!" % option)
            dictMongo[option] = None
    return dictMongo
#function main or startup location of the script for wrting the mongodb data into excel.
#based on some mathematical calculation.
def main():
    """Main Function
    """
    #Read Configuration file for configure mongoDB
    #File Location
    #For accessing the file in the parent folder of the current folder
    script_dir = os.path.dirname(__file__)
    rel_path = "Configuration File\config.ini"
    abs_file_path = os.path.join(script_dir, rel_path)
	#checking for existance of configuration file. 
    try:
        if (os.path.exists(abs_file_path)):
            Config.read(abs_file_path)
        else:
            print("File does not exist!!")
            sys.exit(1)
    except Exception as e:    
        print(e, ":File does not exist")
	#This script loads diffrent variables for 
	#calculation like reaction time, subject id..
    excel_reaction_time_conjunctive = []
    excel_reaction_time_feature = []
    excel_subject_id = []
    excel_region_number = []
    excel_numofobjects = []
    excel_sorted_array_fetaure = tuple()
    excel_sorted_array_totalList = tuple()
    excel_sorted_array_conjunction = tuple()
    excel_sorted_array_totalconjunctionList = tuple()
    feature_data_frame = pd.DataFrame()
    conjunction_data_frame = pd.DataFrame()
    
    #Global variable for connectionstring and database name
	#read from the configuration file for mongodb connection port. 
    Connection_String = ConfigSectionMap("SectionMongoConnection")['connection']
	#read from the configuration file for mongodb database name.
    Database_Name = ConfigSectionMap("SectionMongoDataBase")['database']
	#read from the configuration file for mongodb collection for data manupulation.   
    Excel_Collection_Import = ConfigSectionMap("SectionCollectionNameForExcelImport")['exceldataimport']
    #connect to MongoDB for data visualization
	#Load configuration file input into local variables.
    client = MongoClient(Connection_String)
    db = client[Database_Name]
    account=db[Excel_Collection_Import]
    try:
        excel_subject_id = account.distinct("SubjectID")
        excel_numofobjects = account.distinct("NumOfObjects")
        excel_region_number = account.distinct("RegionNum")
		#here 4 nested for loop is working like a sub-query.
		# 1st for loop is for taking each subjectid, 2nd one is for each region number.
		# 3rd for loop is for number of objects and 4th one is appending the values based on the values coming from the 3 nested for loops.
		# this is for feature search.
        for subject_id in range(len(excel_subject_id)):
            for region_num in range(len(excel_region_number)):
                for numberofobjects in range(len(excel_numofobjects)):
                    for doc in account.find({ "TypeOfSearch":"Feature", "SubjectID" : excel_subject_id[subject_id], "NumOfObjects": excel_numofobjects[numberofobjects], "RegionNum": excel_region_number[region_num]} ):
                        excel_reaction_time_feature.append(doc['ReactionTime'])
                excel_sorted_array_fetaure = [(excel_subject_id[subject_id],int(round(excel_region_number[region_num])),np.array(excel_reaction_time_feature).mean(),"Feature")]
                excel_sorted_array_totalList += tuple(excel_sorted_array_fetaure)
            excel_sorted_array_totalList = sorted(excel_sorted_array_totalList, key=lambda x: float(x[2]), reverse=False)
			#framing the data as datatable for future work.
            fs = pd.DataFrame(excel_sorted_array_totalList, columns=['SubjectID', 'RegionNum', 'ReactionTime', 'TypeOfSearch'])
            feature_data_frame = feature_data_frame.append(fs,ignore_index = True)
            excel_reaction_time_feature.clear()
            excel_sorted_array_fetaure.clear()
            excel_sorted_array_totalList= list(excel_sorted_array_totalList)
            excel_sorted_array_totalList.clear()
            excel_sorted_array_totalList = tuple()
        #Create a Pandas Excel writer using XlsxWriter as the engine.
		#write the whole framed data into excel. #excel sheet location is hardcoded so change it based on requiremnt.
        writer = pd.ExcelWriter('E:\DataInterpretation_Master_Code\AuxiliaryScripts\Configuration File\FeatureSearchSortedData.xlsx', engine='xlsxwriter')
        #Convert the dataframe to an XlsxWriter Excel object.
        feature_data_frame.to_excel(writer, sheet_name='FeatureSearchSortedData')
        #Close the Pandas Excel writer and output the Excel file.
        writer.save()
        writer.close()
		#here 4 nested for loop is working like a sub-query.
		# 1st for loop is for taking each subjectid, 2nd one is for each region number.
		# 3rd for loop is for number of objects and 4th one is appending the values based on the values coming from the 3 nested for loops.
		# this is for conjunctive search.
        for subject_id in range(len(excel_subject_id)):
            for region_num in range(len(excel_region_number)):
                for numberofobjects in range(len(excel_numofobjects)):
                    for doc in account.find({ "TypeOfSearch":"Conjunctive", "SubjectID" : excel_subject_id[subject_id], "NumOfObjects": excel_numofobjects[numberofobjects], "RegionNum": excel_region_number[region_num]} ):
                        excel_reaction_time_conjunctive.append(doc['ReactionTime'])
                excel_sorted_array_conjunction = [(excel_subject_id[subject_id],excel_region_number[region_num],np.array(excel_reaction_time_conjunctive).mean(),"Conjunctive")]
                excel_sorted_array_totalconjunctionList += tuple(excel_sorted_array_conjunction)
            excel_sorted_array_totalconjunctionList = sorted(excel_sorted_array_totalconjunctionList, key=lambda x: float(x[2]), reverse=False)
            cs = pd.DataFrame(excel_sorted_array_totalconjunctionList,columns=['SubjectID', 'RegionNum', 'ReactionTime', 'TypeOfSearch'])
			#framing the data as datatable for future work.
            conjunction_data_frame = conjunction_data_frame.append(cs,ignore_index = True)
            excel_reaction_time_conjunctive.clear()
            excel_sorted_array_conjunction.clear()
            excel_sorted_array_totalconjunctionList= list(excel_sorted_array_totalconjunctionList)
            excel_sorted_array_totalconjunctionList.clear()
            excel_sorted_array_totalconjunctionList = tuple()            
        #Create a Pandas Excel writer using XlsxWriter as the engine.
		#write the whole framed data into excel. #excel sheet location is hardcoded so change it based on requiremnt.
        writer = pd.ExcelWriter('E:\DataInterpretation_Master_Code\AuxiliaryScripts\Configuration File\ConjunctiveSearchSortedData.xlsx', engine='xlsxwriter')
        #Convert the dataframe to an XlsxWriter Excel object.
        conjunction_data_frame.to_excel(writer, sheet_name='ConjunctiveSearchSortedData')
        #Close the Pandas Excel writer and output the Excel file.
        writer.save()
    except Exception as err:
        print(err)
#Application start-up point 
#main function call 
if __name__ == "__main__":
    main()
    print("Data Write Complete!!")
